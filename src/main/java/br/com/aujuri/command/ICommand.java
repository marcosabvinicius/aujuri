package br.com.aujuri.command;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;

public interface ICommand {
	public Resultado execute(EntidadeDominio entidadeDominio);
}
