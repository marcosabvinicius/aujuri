package br.com.aujuri.command;

import org.springframework.stereotype.Component;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;

@Component
public class CommandAlterar extends AbstractCommand {

	@Override
	public Resultado execute(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return this.fachada.alterar(entidadeDominio);
	}

}
