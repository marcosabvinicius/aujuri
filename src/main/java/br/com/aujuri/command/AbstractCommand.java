package br.com.aujuri.command;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.aujuri.fachada.IFachada;

public abstract class AbstractCommand implements ICommand{
	@Autowired
	protected IFachada fachada; //Todos os objetos de Command terão objetos do tipo fachada
}
