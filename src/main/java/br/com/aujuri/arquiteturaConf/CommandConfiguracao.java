package br.com.aujuri.arquiteturaConf;

import java.lang.annotation.Documented;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.aujuri.command.CommandAlterar;
import br.com.aujuri.command.CommandConsultar;
import br.com.aujuri.command.CommandExcluir;
import br.com.aujuri.command.CommandSalvar;
import br.com.aujuri.command.ICommand;
import br.com.aujuri.dao.ClientePFDAO;
import br.com.aujuri.dao.FuncionarioDAO;
import br.com.aujuri.dao.IDAO;
import br.com.aujuri.model.ClientePessoaFisica;
import br.com.aujuri.model.Funcionario;

@Configuration
public class CommandConfiguracao {
	@Autowired
	private CommandSalvar commandSalvar;
	@Autowired
	private CommandAlterar commandAlterar;
	@Autowired
	private CommandConsultar commandConsultar;
	@Autowired
	private CommandExcluir commandExcluir;

	
	//Esse método monta um objeto do tipo Map, o qual preencherá os commands a serem executados em todas as controllers
	@Bean
	public Map<String,ICommand> commands()
	{		
		Map<String, ICommand> commands = new HashMap<>();
		commands.put("SALVAR", commandSalvar);
		commands.put("ALTERAR", commandAlterar);
		commands.put("CONSULTAR", commandConsultar);
		commands.put("EXCLUIR", commandExcluir);
		return commands;
	}
	
}
