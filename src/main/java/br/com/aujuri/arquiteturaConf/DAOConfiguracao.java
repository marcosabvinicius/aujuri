package br.com.aujuri.arquiteturaConf;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.aujuri.dao.AtividadeDAO;
import br.com.aujuri.dao.ClientePFDAO;
import br.com.aujuri.dao.FuncionarioDAO;
import br.com.aujuri.dao.IDAO;
import br.com.aujuri.dao.LoginDAO;
import br.com.aujuri.model.Atividade;
import br.com.aujuri.model.ClientePessoaFisica;
import br.com.aujuri.model.ClientePessoaJuridica;
import br.com.aujuri.model.Funcionario;
import br.com.aujuri.model.Login;
import br.com.aujuri.model.Movimentacoes;
import br.com.aujuri.model.Prazos;
import br.com.aujuri.model.Processo;
import br.com.aujuri.model.Publicacao;
import br.com.aujuri.model.Recurso;

@Configuration
public class DAOConfiguracao {

	@Autowired
	@Qualifier("FuncionarioDAO")
	private IDAO funcionarioDAO;
	@Autowired
	@Qualifier("ClientePFDAO")
	private IDAO clientePFDAO;
	@Autowired
	@Qualifier("LoginDAO")
	private IDAO loginDAO;
	@Autowired
	@Qualifier("ClientePJDAO")
	private IDAO clientePJDAO;
	@Autowired
	@Qualifier("ProcessoDAO")
	private IDAO processoDAO;
	@Autowired
	@Qualifier("AtividadeDAO")
	private IDAO AtividadeDAO;
	@Autowired
	@Qualifier("PrazosDAO")
	private IDAO prazosDAO;
	@Autowired
	@Qualifier("RecursoDAO")
	private IDAO recursoDAO;
	
	//Retorna um mapa de DAO's, utilizados na fachada. Dessa forma, a fachada fica genérica
	@Bean
	public Map<String, IDAO> mapaDAO()
	{
		Map<String, IDAO> daos = new HashMap<>();
		daos.put(Funcionario.class.getName(), funcionarioDAO);
		daos.put(ClientePessoaFisica.class.getName(), clientePFDAO);
		daos.put(ClientePessoaJuridica.class.getName(), clientePJDAO);
		daos.put(Login.class.getName(), loginDAO);
		daos.put(Processo.class.getName(), processoDAO);
		daos.put(Atividade.class.getName(), AtividadeDAO);
		daos.put(Prazos.class.getName(), prazosDAO);
		daos.put(Recurso.class.getName(), recursoDAO);
		return daos;
	}
}
