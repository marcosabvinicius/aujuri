package br.com.aujuri.fachada;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;

public interface IFachada {
	public Resultado salvar(EntidadeDominio entidadeDominio);
	public Resultado alterar(EntidadeDominio entidadeDominio);
	public Resultado excluir(EntidadeDominio entidadeDominio);
	public Resultado consultar(EntidadeDominio entidadeDominio);
}
