package br.com.aujuri.fachada;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import br.com.aujuri.application.Resultado;
import br.com.aujuri.dao.ClientePFDAO;
import br.com.aujuri.dao.FuncionarioDAO;
import br.com.aujuri.dao.IDAO;
import br.com.aujuri.model.ClientePessoaFisica;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Funcionario;

@Component
public class Fachada implements IFachada{
	
	@Resource
	private Map<String, IDAO> mapaDAO;
		
	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		System.out.println("cheguei na fachada");
		//Como o mapa de DAOS está genérico, basta informar o tipo de objeto que é que retornará o DAO correto
		IDAO dao = mapaDAO.get(entidadeDominio.getClass().getName()); 
		Resultado resultado = dao.salvar(entidadeDominio);
		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		IDAO dao = mapaDAO.get(entidadeDominio.getClass().getName());
		Resultado resultado = dao.alterar(entidadeDominio);
		return resultado;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		IDAO dao = mapaDAO.get(entidadeDominio.getClass().getName());
		Resultado resultado = dao.excluir(entidadeDominio);
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		IDAO dao = mapaDAO.get(entidadeDominio.getClass().getName());
		Resultado resultado = dao.consultar(entidadeDominio);
		return resultado;
	}

}
