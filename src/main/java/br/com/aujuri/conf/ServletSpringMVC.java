package br.com.aujuri.conf;

import javax.servlet.Filter;

import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import br.com.aujuri.arquiteturaConf.CommandConfiguracao;
import br.com.aujuri.arquiteturaConf.DAOConfiguracao;
import br.com.aujuri.util.FileSaver;


import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration.Dynamic;

public class ServletSpringMVC extends AbstractAnnotationConfigDispatcherServletInitializer {
	//Todas as classes de configurações devem estar setadas aqui, para que o Spring reconheça a classe.
	@Override
	protected Class<?>[] getRootConfigClasses() {
		// TODO Auto-generated method stub
		return new Class[] {JPAConfiguration.class,	AppWebConfiguration.class, WebMvcConfig.class, CommandConfiguracao.class,
				DAOConfiguracao.class,SecurityConfiguration.class, SpringSecurityFilterConfiguration.class}; 
		
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		// TODO Auto-generated method stub
		return new Class[] {};
	}

	@Override
	protected String[] getServletMappings() {
		// TODO Auto-generated method stub
		return new String[] {"/"};
	}
	@Override
	protected Filter[] getServletFilters() {
		// TODO Auto-generated method stub
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		return new Filter[]{characterEncodingFilter};
	}
	
	/*
	 *  Nesta classe, sobrescrevi um método chamado customizeRegistration que recebe um objeto do tipo Dynamic 
	 *  que se chama de registration. Neste objeto, tem o método setMultipartConfig que requer um objeto do 
	 *  tipo MultipartConfigElement. O MultipartConfigElement espera receber uma String que configure o arquivo. 
	 *  Não usei nenhuma configuração para o arquivo, querp receber este do jeito que vier. Passei então uma String vazia.
	 */
	@Override
	protected void customizeRegistration(Dynamic registration) {
	       registration.setMultipartConfig(new MultipartConfigElement(""));
	}
}
