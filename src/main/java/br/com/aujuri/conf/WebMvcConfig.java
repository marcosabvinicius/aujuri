package br.com.aujuri.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	  
		@Override
		public void addResourceHandlers(final ResourceHandlerRegistry registry) {
			registry.addResourceHandler("/resources/**").addResourceLocations("/resources/"); //Diretório onde os arquivos CSS e JS devem estar contidos
		}
	
		@Override
	    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
	        configurer.enable();
	    }
		
		@Bean
		public OpenEntityManagerInViewInterceptor getOpenEntityManagerInViewInterceptor() { 
		    return new OpenEntityManagerInViewInterceptor(); //Permite o acesso do entityManager na JSP.
		}
		
		@Override
		public void addInterceptors(InterceptorRegistry registry) {
		    registry.addWebRequestInterceptor(getOpenEntityManagerInViewInterceptor());
		}
}
