package br.com.aujuri.conf;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.datetime.DateFormatterRegistrar;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import br.com.aujuri.command.CommandAlterar;
import br.com.aujuri.command.CommandSalvar;
import br.com.aujuri.controller.ClientePFController;
import br.com.aujuri.controller.ProcessoController;
import br.com.aujuri.dao.FuncionarioDAO;
import br.com.aujuri.dao.ProcessoDAO;
import br.com.aujuri.fachada.Fachada;
import br.com.aujuri.model.Processo;
import br.com.aujuri.util.FileSaver;
import br.com.aujuri.arquiteturaConf.CommandConfiguracao;

@ComponentScan(basePackageClasses = {FuncionarioDAO.class,ClientePFController.class,Fachada.class, CommandSalvar.class, CommandAlterar.class, CommandConfiguracao.class, Processo.class, FileSaver.class, ProcessoDAO.class})
@EnableWebMvc
public class AppWebConfiguration {

	//Método que define onde as JSP deverão estar armazenadas
	@Bean
	public InternalResourceViewResolver internalResourceViewResolver()
	{
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/"); //Prefixo
		resolver.setSuffix(".jsp");	//Sufixo
		return resolver;
	}
	
	@Bean
	public MessageSource messageSource()
	{
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/WEB-INF/messages");
		messageSource.setDefaultEncoding("UTF-8");
		messageSource.setCacheSeconds(1);
		return messageSource;
	}
	
	@Bean //Método que define o padrão em que as datas devem estar
	public FormattingConversionService mvcConversionService()
	{
		DefaultFormattingConversionService defaultFormattingConversionService = new DefaultFormattingConversionService();
		DateFormatterRegistrar dateFormatterRegistrar = new DateFormatterRegistrar();
		dateFormatterRegistrar.setFormatter(new DateFormatter("dd/MM/yyyy")); //As datas devem estar formatadas em dia/Mes/Ano
		dateFormatterRegistrar.registerFormatters(defaultFormattingConversionService);
		return defaultFormattingConversionService;
	}
	
	 @Bean
	 public MultipartResolver multipartResolver(){
	    return new StandardServletMultipartResolver();
	 }
}
