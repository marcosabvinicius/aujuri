package br.com.aujuri.application;

import java.util.List;

import br.com.aujuri.model.EntidadeDominio;
//Classe responsável por coletar as mensagens e entidades durante o fluxo da arquitetura do projeto
public class Resultado {
	private String mensagemSimples; //Caso seja necessário exibir apenas uma mensagem, utilizar essa variável
	private List<EntidadeDominio> entidadeDominios; //Lista de entidades retornados do DAO
	private List<String> listaMensagens; //Lista de mensagens a ser exibida na JSP
	private boolean status;
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMensagemSimples() {
		return mensagemSimples;
	}
	public void setMensagemSimples(String mensagemSimples) {
		this.mensagemSimples = mensagemSimples;
	}
	public List<EntidadeDominio> getEntidadeDominios() {
		return entidadeDominios;
	}
	public void setEntidadeDominios(List<EntidadeDominio> entidadeDominios) {
		this.entidadeDominios = entidadeDominios;
	}
	public List<String> getListaMensagens() {
		return listaMensagens;
	}
	public void setListaMensagens(List<String> listaMensagens) {
		this.listaMensagens = listaMensagens;
	}
}
