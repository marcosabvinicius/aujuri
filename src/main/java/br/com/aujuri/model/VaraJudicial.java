package br.com.aujuri.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class VaraJudicial extends EntidadeDominio{
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idVaraJudicial;
	private String nomeVaraJudicial;
	private String tipoVaraJudicial;
	@ElementCollection
	private List<Telefone> telefones;
	@ManyToOne
	private Forum forum;
	
	public Forum getForum() {
		return forum;
	}
	public void setForum(Forum forum) {
		this.forum = forum;
	}
	public int getIdVaraJudicial() {
		return idVaraJudicial;
	}
	public void setIdVaraJudicial(int idVaraJudicial) {
		this.idVaraJudicial = idVaraJudicial;
	}
	public String getNomeVaraJudicial() {
		return nomeVaraJudicial;
	}
	public void setNomeVaraJudicial(String nomeVaraJudicial) {
		this.nomeVaraJudicial = nomeVaraJudicial;
	}
	public String getTipoVaraJudicial() {
		return tipoVaraJudicial;
	}
	public void setTipoVaraJudicial(String tipoVaraJudicial) {
		this.tipoVaraJudicial = tipoVaraJudicial;
	}
	public List<Telefone> getTelefones() {
		return telefones;
	}
	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}
}
