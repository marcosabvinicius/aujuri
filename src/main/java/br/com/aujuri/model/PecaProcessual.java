package br.com.aujuri.model;

import javax.persistence.Embeddable;


@Embeddable
public class PecaProcessual extends EntidadeDominio{
	
	private String nomePeca;
	private String paragrafo;
	private String descricao;
	private String caminhoArquivo;
	
	public String getNomePeca() {
		return nomePeca;
	}
	public void setNomePeca(String nomePeca) {
		this.nomePeca = nomePeca;
	}
	public String getParagrafo() {
		return paragrafo;
	}
	public void setParagrafo(String paragrafo) {
		this.paragrafo = paragrafo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getCaminhoArquivo() {
		return caminhoArquivo;
	}
	public void setCaminhoArquivo(String caminhoArquivo) {
		this.caminhoArquivo = caminhoArquivo;
	}
}
