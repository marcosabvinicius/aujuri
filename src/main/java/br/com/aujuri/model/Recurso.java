package br.com.aujuri.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Recurso extends EntidadeDominio{
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		int idRecurso;
		String nomeRecurso;
		String paragrafo;
		@Lob
		String descricao;
		String tipoProcesso;
		public int getIdRecurso() {
			return idRecurso;
		}
		public void setIdRecurso(int idRecurso) {
			this.idRecurso = idRecurso;
		}
		public String getNomeRecurso() {
			return nomeRecurso;
		}
		public void setNomeRecurso(String nomeRecurso) {
			this.nomeRecurso = nomeRecurso;
		}
		public String getParagrafo() {
			return paragrafo;
		}
		public void setParagrafo(String paragrafo) {
			this.paragrafo = paragrafo;
		}
		public String getDescricao() {
			return descricao;
		}
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		public String getTipoProcesso() {
			return tipoProcesso;
		}
		public void setTipoProcesso(String tipoProcesso) {
			this.tipoProcesso = tipoProcesso;
		}
		
}//class
