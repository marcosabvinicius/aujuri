package br.com.aujuri.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Processo extends EntidadeDominio{
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idProcesso")
	private int idProcesso;
	private String numeroProcesso;
	private String parteContraria;
	private String statusProcesso;
	@ManyToMany
	private List<Funcionario> funcionarios;
	@ElementCollection
	private List<PecaProcessual> pecas;
	@ElementCollection
	private List<Agenda> agendas;
	@ElementCollection
	private List<Publicacao> publicacoes;
	@OneToMany
	private List<VaraJudicial> varaJudiciais;	
	public int getIdProcesso() {
		return idProcesso;
	}
	public void setIdProcesso(int idProcesso) {
		this.idProcesso = idProcesso;
	}
	public String getNumeroProcesso() {
		return numeroProcesso;
	}
	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}
	public String getParteContraria() {
		return parteContraria;
	}
	public void setParteContraria(String parteContraria) {
		this.parteContraria = parteContraria;
	}
	public String getStatusProcesso() {
		return statusProcesso;
	}
	public void setStatusProcesso(String statusProcesso) {
		this.statusProcesso = statusProcesso;
	}
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	public List<PecaProcessual> getPecas() {
		return pecas;
	}
	public void setPecas(List<PecaProcessual> pecas) {
		this.pecas = pecas;
	}
	public List<Agenda> getAgendas() {
		return agendas;
	}
	public void setAgendas(List<Agenda> agendas) {
		this.agendas = agendas;
	}
	public List<Publicacao> getPublicaoes() {
		return publicacoes;
	}
	public void setPublicoes(List<Publicacao> publicaoes) {
		this.publicacoes = publicaoes;
	}
	public List<VaraJudicial> getVaraJudiciais() {
		return varaJudiciais;
	}
	public void setVaraJudiciais(List<VaraJudicial> varaJudiciais) {
		this.varaJudiciais = varaJudiciais;
	}
	
	
}
