package br.com.aujuri.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="idPessoa")
public class ClientePessoaFisica extends PessoaFisica implements ICliente{
	
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Processo> processos;

	public List<Processo> getProcessos() {
		return processos;
	}

	public void setProcessos(List<Processo> processos) {
		this.processos = processos;
	}
}
