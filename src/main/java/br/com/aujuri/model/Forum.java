package br.com.aujuri.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Forum extends EntidadeDominio{
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idForum;
	private String nomeForum;
	@Embedded
	private Endereco endereco;
	
	public String getNomeForum() {
		return nomeForum;
	}
	public void setNomeForum(String nomeForum) {
		this.nomeForum = nomeForum;
	}

	public int getIdForum() {
		return idForum;
	}
	public void setIdForum(int idForum) {
		this.idForum = idForum;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
}
