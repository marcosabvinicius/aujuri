 package br.com.aujuri.model;

import javax.persistence.Entity;

@Entity
public class Funcionario extends PessoaFisica{
	private String numeroOAB;
	private TipoFuncionario tipoFuncionario;
	public String getNumeroOAB() {
		return numeroOAB;
	}
	public void setNumeroOAB(String numeroOAB) {
		this.numeroOAB = numeroOAB;
	}
	public TipoFuncionario getTipoFuncionario() {
		return tipoFuncionario;
	}
	public void setTipoFuncionario(TipoFuncionario tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}
}
