package br.com.aujuri.model;

import java.util.Calendar;

import javax.persistence.Embeddable;

@Embeddable
public class Agenda extends EntidadeDominio{
	private Calendar prazoPublicacao;

	public Calendar getPrazoPublicacao() {
		return prazoPublicacao;
	}

	public void setPrazoPublicacao(Calendar prazoPublicacao) {
		this.prazoPublicacao = prazoPublicacao;
	}
}
