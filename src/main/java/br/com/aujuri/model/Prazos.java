package br.com.aujuri.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Prazos extends EntidadeDominio{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int idPrazo;	
	String tipoProcesso;
	String paragrafo;
	String prazo;
	@Lob
	String descricao;
	
	public String getTipoProcesso() {
		return tipoProcesso;
	}
	public void setTipoProcesso(String tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}
	public String getParagrafo() {
		return paragrafo;
	}
	public void setParagrafo(String paragrafo) {
		this.paragrafo = paragrafo;
	}
	public String getPrazo() {
		return prazo;
	}
	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
