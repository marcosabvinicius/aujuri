package br.com.aujuri.model;

import javax.persistence.Embeddable;

@Embeddable
public class Telefone extends EntidadeDominio{
	private String numeroTelefone;
	private String ddd;
	public String getNumeroTelefone() {
		return numeroTelefone;
	}
	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}
	public String getDdd() {
		return ddd;
	}
	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
}
