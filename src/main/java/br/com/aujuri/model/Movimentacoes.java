package br.com.aujuri.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;


/*
 *Essa estratégia de mapeamento define que seja gerada uma tabela por classe concreta. Cada tabela possui seus atributos, incluindo os atributos herdados da superclasse.
 */

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Movimentacoes extends EntidadeDominio{
	@Id
	@GeneratedValue
	@Column(name="idMovimentacao")
	private int idMovimentacao; 
	@Lob
	private String movimentacao;

	public String getMovimentacao() {
		return movimentacao;
	}

	public void setMovimentacao(String movimentacao) {
		this.movimentacao = movimentacao;
	}

	public int getIdMovimentacao() {
		return idMovimentacao;
	}

	public void setIdMovimentacao(int idMovimentacao) {
		this.idMovimentacao = idMovimentacao;
	}
}
