package br.com.aujuri.dao;

import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.ClientePessoaFisica;
import br.com.aujuri.model.EntidadeDominio;


@Repository
@Transactional
@Qualifier("ClientePFDAO")
public class ClientePFDAO extends AbstractDAO{
	
	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		System.out.println("Cheguei no DAO!");
		ClientePessoaFisica clientePessoaFisica = (ClientePessoaFisica) entidadeDominio;
		System.out.println(entityManager);
		this.entityManager.persist(clientePessoaFisica);
		Resultado resultado = new Resultado();
		resultado.setMensagemSimples("Cliente cadastrado com sucesso!");
		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		ClientePessoaFisica clientePessoaFisica = (ClientePessoaFisica) entidadeDominio;
		if(clientePessoaFisica.getProcessos() != null) //Objeto foi instanciado?
		{
			ClientePessoaFisica clienteAux;
			clienteAux = entityManager.find(ClientePessoaFisica.class, clientePessoaFisica.getIdPessoa());
			clienteAux.setProcessos(clientePessoaFisica.getProcessos());
			entidadeDominio = clienteAux;
		}
		entityManager.merge(entidadeDominio);
		resultado = new Resultado();
		resultado.setMensagemSimples("Cliente alterado com sucesso!");
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		resultado = new Resultado();
		ClientePessoaFisica clientePessoaFisica = (ClientePessoaFisica) entidadeDominio;
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(ClientePessoaFisica.class);
		if(clientePessoaFisica.getIdPessoa() != 0) //Foi informado ID? Caso sim, retorne o objeto com o ID solicitado
			criteria.add(Restrictions.idEq(clientePessoaFisica.getIdPessoa()));
		else if(clientePessoaFisica.getNome() != null) //Foi informado nome? Caso sim, retorne todos os objetos com o nome solicitado
			criteria.add(Restrictions.like("nome", clientePessoaFisica.getNome()));
		else if(clientePessoaFisica.getCpf() != null) //Foi informado CPF? Caso sim, retorne todos os objetos com o CPJ solicitado
			criteria.add(Restrictions.like("cpf", clientePessoaFisica.getCpf()));
		else if(clientePessoaFisica.getRg() != null) //Foi informado RG? Caso sim, retorne todos os objetos com o CPJ solicitado
			criteria.add(Restrictions.like("rg", clientePessoaFisica.getRg()));
		resultado.setEntidadeDominios(criteria.list());
		return resultado;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		resultado = new Resultado();
		ClientePessoaFisica clientePessoaFisica = (ClientePessoaFisica) entityManager.merge(entidadeDominio);
		entityManager.remove(clientePessoaFisica);
		resultado.setMensagemSimples("Cliente excluído com sucesso!");
		return resultado;
	}

}
