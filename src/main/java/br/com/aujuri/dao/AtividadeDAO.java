package br.com.aujuri.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.Atividade;
import br.com.aujuri.model.EntidadeDominio;

@Repository
@Transactional
@Qualifier("AtividadeDAO")
public class AtividadeDAO implements IDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		entityManager.persist(entidadeDominio);
		return null;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		entityManager.merge(entidadeDominio);
		Resultado resultado = new Resultado();
		resultado.setMensagemSimples("Preço atualizado com sucesso!");
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Atividade.class);
		if(entidadeDominio != null)
		{
			Atividade atividade = (Atividade) entidadeDominio;
			if(atividade.getId() != 0)
				criteria.add(Restrictions.idEq(atividade.getId()));
		}
		Resultado resultado = new Resultado();
		resultado.setEntidadeDominios(criteria.list());
		return resultado;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return null;
	}

}
