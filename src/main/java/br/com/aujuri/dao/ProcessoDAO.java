package br.com.aujuri.dao;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Processo;

@Repository
@Transactional
@Qualifier("ProcessoDAO")
public class ProcessoDAO extends AbstractDAO implements IDAO {

	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		entityManager.merge(entidadeDominio);
		Resultado resultado = new Resultado();
		resultado.setMensagemSimples("Processo cadastrado com sucesso");
		return null;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Processo.class);
		Processo processo = (Processo) entidadeDominio;
		if(processo != null)
		{
			if(processo.getIdProcesso() != 0)
				criteria.add(Restrictions.idEq(processo.getIdProcesso()));
			if(processo.getNumeroProcesso() != null)
				criteria.add(Restrictions.like("numeroProcesso","%"+processo.getNumeroProcesso()+"%"));
		}
		Resultado resultado = new Resultado();
		resultado.setEntidadeDominios(criteria.list());
		return resultado;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return null;
	}

}
