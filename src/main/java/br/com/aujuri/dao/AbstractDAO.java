package br.com.aujuri.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.aujuri.application.Resultado;

public abstract class AbstractDAO implements IDAO{
	@PersistenceContext
	protected EntityManager entityManager; //Todas os DAO's possuirão entityManager, objeto responsável por executar comandos no BD

	protected Resultado resultado;
}
