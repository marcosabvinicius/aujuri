package br.com.aujuri.dao;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;

public interface IDAO {
	public Resultado salvar(EntidadeDominio entidadeDominio);
	public Resultado alterar(EntidadeDominio entidadeDominio);
	public Resultado consultar(EntidadeDominio entidadeDominio);
	public Resultado excluir(EntidadeDominio entidadeDominio);
}
