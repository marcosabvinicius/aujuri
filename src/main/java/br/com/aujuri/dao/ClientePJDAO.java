package br.com.aujuri.dao;

import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.ClientePessoaJuridica;
import br.com.aujuri.model.EntidadeDominio;

@Repository
@Transactional
@Qualifier("ClientePJDAO")
public class ClientePJDAO extends AbstractDAO {
	
	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		System.out.println("Cheguei no DAO!");
		ClientePessoaJuridica clientePessoaJuridica = (ClientePessoaJuridica) entidadeDominio;
		System.out.println(entityManager);
		this.entityManager.persist(clientePessoaJuridica);
		Resultado resultado = new Resultado();
		resultado.setMensagemSimples("Cliente cadastrado com sucesso!");
		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		entityManager.merge(entidadeDominio);
		resultado = new Resultado();
		resultado.setMensagemSimples("Cliente alterado com sucesso!");
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		resultado = new Resultado();
		ClientePessoaJuridica clientePessoaJuridica = (ClientePessoaJuridica) entidadeDominio;
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(ClientePessoaJuridica.class);
		if(clientePessoaJuridica.getIdPessoa() != 0) //Foi informado ID? Caso sim, retorne todos os objetos com o ID solicitado
		{
			criteria.add(Restrictions.idEq(clientePessoaJuridica.getIdPessoa()));
		}
		resultado.setEntidadeDominios(criteria.list());
		return resultado;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		resultado = new Resultado();
		ClientePessoaJuridica clientePessoaJuridica = (ClientePessoaJuridica) entityManager.merge(entidadeDominio);
		entityManager.remove(clientePessoaJuridica);
		resultado.setMensagemSimples("Cliente excluído com sucesso!");
		return resultado;
	}

}
