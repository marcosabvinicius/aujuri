package br.com.aujuri.dao;


import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Funcionario;

@Repository
@Transactional
@Qualifier("FuncionarioDAO")
public class FuncionarioDAO extends AbstractDAO{

	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		entityManager.merge(entidadeDominio);
		resultado = new Resultado();
		resultado.setMensagemSimples("Funcionario cadastrado com sucesso!");
		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		entityManager.merge(entidadeDominio);
		resultado = new Resultado();
		resultado.setMensagemSimples("Funcionario alterado com sucesso!");
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		resultado = new Resultado();
		Funcionario funcionario = (Funcionario) entidadeDominio;
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Funcionario.class);
		if(funcionario.getIdPessoa() != 0) //Foi informado ID? sim, retorne todos os objetos com o ID solicitado
		{
			criteria.add(Restrictions.idEq(funcionario.getIdPessoa()));
		}
		resultado.setEntidadeDominios(criteria.list());
		return resultado;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		resultado = new Resultado();
		Funcionario funcionario = (Funcionario) entityManager.merge(entidadeDominio);
		entityManager.remove(funcionario);
		resultado.setMensagemSimples("Funcionario excluido com sucesso!");
		return resultado;
	}

}
