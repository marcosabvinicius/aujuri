package br.com.aujuri.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Login;
import br.com.aujuri.model.PessoaFisica;

@Repository
@Qualifier("LoginDAO")
public class LoginDAO extends AbstractDAO implements UserDetailsService{

	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return null;
	}

	//Este método é utilizado durante o processo de login na aplicação, sendo este o responsável por realizar a autenticação. Foi implementado para considerar o login com e-mail e senha
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		List<PessoaFisica> logins = entityManager.createQuery("select p from PessoaFisica p where p.login.email = :email", PessoaFisica.class) //Retorne o objeto que contenha o e-mail solicitado
				.setParameter("email", email).getResultList();
		if(logins.isEmpty())
			throw new UsernameNotFoundException("O usuário " + email + " não foi encontrado");
		return logins.get(0).getLogin();
	}

}
