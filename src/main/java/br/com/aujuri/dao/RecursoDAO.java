package br.com.aujuri.dao;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Prazos;
import br.com.aujuri.model.Recurso;

@Repository
@Transactional
@Qualifier("RecursoDAO")
public class RecursoDAO extends AbstractDAO{
	
	@Override
	public Resultado salvar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return resultado;
	}

	@Override
	public Resultado alterar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return resultado;
	}

	@Override
	public Resultado consultar(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		resultado = new Resultado();
		Recurso recurso = (Recurso) entidadeDominio;
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Recurso.class);
		resultado.setEntidadeDominios(criteria.list());
		return resultado;
	}

	@Override
	public Resultado excluir(EntidadeDominio entidadeDominio) {
		// TODO Auto-generated method stub
		return resultado;
	}
}
