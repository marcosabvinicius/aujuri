package br.com.aujuri.util;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileSaver {

	@Autowired
    private HttpServletRequest request;

   	//basefolder é o local onde o arquivo será salvo
	//file é o arquivo
	public String write(String baseFolder, MultipartFile file){
 
    	try {
    		
    		String realPath = request.getServletContext().getRealPath("/" + baseFolder);
    		System.out.println(realPath);
    		System.out.println(file);
            String path = file.getOriginalFilename();
            System.out.println("Nome original do arquivo: "  + path);
            file.transferTo(new File(path));
            System.out.println("Caminho + Nome: " + baseFolder + "/" + file.getOriginalFilename());
            return baseFolder + "/" + file.getOriginalFilename();

        } catch (IllegalStateException | IOException e) {
            throw new RuntimeException(e);
        }//catch
    }//write
}//FileSaver
