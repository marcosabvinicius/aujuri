package br.com.aujuri.util;

import java.util.ArrayList;
import java.util.List;

import br.com.aujuri.model.Atividade;

public class ListaAtividadesPreco {

	public static List<Atividade> obterListaAtividade()
	{
		List<Atividade> atividades = new ArrayList<>();
		Atividade atividade;
		
		atividade = new Atividade();
		atividade.setDescricao("Procedimento ordinário: proposição ou defesa ");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Procedimento sumário: proposição ou defesa");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Cumprimento de sentença");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Impugnação ao cumprimento de sentença");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Execução de título extrajudicial");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Impugnação/embargos à execução de título extrajudicial");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Impugnação/embargos à penhora, à arrematação, à adjudicação, ao leilão, de títulos judiciais e\r\n" + 
				"extrajudiciais ");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Processo cautelar específico: incidental ou preparatório");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Processo cautelar inominado: incidental ou preparatório");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Consignação em pagamento");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Depósito");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Anulação e substituição de título ao portador");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Prestação de contas");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Móvel");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Imóvel: interdito proibitório – manutenção – reintegração");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Nunciação de obra nova");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Usucapião");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Embargos de terceiro");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Habilitação");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Restauração de autos");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Das vendas a crédito com reserva de domínio");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Do Juízo arbitral");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Da ação monitória");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Desapropriação direta");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Desapropriação indireta");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Inominada");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação de retificação de registro público");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Alvará judicial");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação de constituição, extinção de usufruto ou fideicomisso");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Mandado de segurança");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação ordinária de despejo");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação renovatória de locação");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação de revisão e/ou arbitramento de aluguel");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação de consignação de aluguel");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Atos/acompanhamento despejo/reintegração");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação de dissolução de sociedade");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação de cancelamento de protesto");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Mandado de injunção");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Habeas data");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação negatória ou de abstenção de uso de matéria de propriedade intelectual");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação indenizadora por prejuízos decorrentes de contratação ou crime em matéria de propriedade\r\n" + 
				"intelectual ");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Ação negatória ou de abstenção de uso de matéria de propriedade industrial");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Busca e apreensão em matéria de propriedade intelectual e industrial");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Procedimentos sobre propriedade intelectual: depósito de marca ou patente, oposição, recursos,\r\n" + 
				"revisão, caducidade, nulidade etc.");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Análise da documentação e pedido de registro de loteamento ou desmembramento, por grupo de\r\n" + 
				"dez lotes");
		atividades.add(atividade);
		
		atividade = new Atividade();
		atividade.setDescricao("Opção de nacionalidade");
		atividades.add(atividade);		
		
		return atividades;
	}
}
