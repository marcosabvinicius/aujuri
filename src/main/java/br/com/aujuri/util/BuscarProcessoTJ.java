package br.com.aujuri.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.htmlunit.MockMvcWebClientBuilder;
import org.springframework.web.context.WebApplicationContext;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;import com.gargoylesoftware.htmlunit.javascript.JavaScriptEngine;

import br.com.aujuri.conf.AppWebConfiguration;
import br.com.aujuri.conf.WebMvcConfig;
import br.com.aujuri.model.Publicacao;
import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.Source;

@WebAppConfiguration
@ContextConfiguration(classes ={WebMvcConfig.class})
public class BuscarProcessoTJ {
	
	//Objeto responsável 
	public static List<Publicacao> obterPublicacoes(String numeroProcesso) throws FailingHttpStatusCodeException, MalformedURLException, IOException, ParseException, KeyManagementException, NoSuchAlgorithmException
	{
		SSLUtil.turnOffSslChecking();
		List<Publicacao> publicacoes = new ArrayList<>(); //Lista que armazenará cada publicação obtida no site do TJ-SP
		Publicacao publicacao;
		WebClient webClient = new WebClient(BrowserVersion.BEST_SUPPORTED); //Utilizar a versão de Browser mais adequada ao site
		webClient.getOptions().setUseInsecureSSL(true);
    	HtmlPage htmlPage = webClient.getPage(new URL("https://esaj.tjsp.jus.br/cpopg/open.do")); //Obter o HTML do site do TJ
    	HtmlForm form = htmlPage.getFormByName("consultarProcessoForm"); //Obter o formulário que realiza a consulta do processo
    	HtmlTextInput htmlTextInput = form.getInputByName("dadosConsulta.valorConsulta"); //Pegar o objeto responsável por armazenar o número do projeto
    	htmlTextInput.setValueAttribute(numeroProcesso); //Preencher o campo com o número informado no formulário do Aujuri
    	HtmlInput htmlSubmitInput = form.getInputByName("pbEnviar"); //Efetuar a consulta no site do TJ com o número do processo
    	Page resposta = htmlSubmitInput.click(); //Ao submeter, obter resposta do site
    	System.out.println(resposta.getUrl());
    	//Parte com Jericho
    	Source source = new Source(resposta.getUrl()); //Com a URL gerada, coletar os campos pertinentes as publicações do TJ
    	Element elementById = source.getElementById("tabelaUltimasMovimentacoes"); //Obter tabela com as movimentações do processo
    	List<Element> childElements = elementById.getChildElements(); //Obter cada linha contendo as publicações
    	for(Element e: childElements)
    	{
    		List<Element> allElements = e.getAllElements("tr"); //Separar as linhas da tabela
    		for(Element e2 : allElements)
    		{
    			int i=0;
    			List<Element> allElements2 = e2.getAllElements("td"); //Separar os dados da tabela
    			publicacao = new Publicacao();
    			for(Element e3 : allElements2)
    			{
    				if(i==0)
    				{
    					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); //Formatar a data do TJ para o formato padrão AUJURI
    					Date date = sdf.parse(e3.getTextExtractor().toString());
    					Calendar calendar = sdf.getCalendar();
    					calendar.setTime(date);
    					publicacao.setDataPublicacao(calendar);
    					
    				}
    				else if(i==2)
    					publicacao.setConteudo(e3.getTextExtractor().toString());
    				i++;
    			}
    			publicacoes.add(publicacao); //Com a linha lida, adicionar a lista
    		}
    	}
    	return publicacoes; //Retornar todas as publicações obtidas
	}
}
