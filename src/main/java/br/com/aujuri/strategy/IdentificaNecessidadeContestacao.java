package br.com.aujuri.strategy;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Publicacao;

//O próposito dessa classe é identificar quais podem ser os próximos possíveis passos
public class IdentificaNecessidadeContestacao implements IStrategy {
	
	@Override
	public Resultado processar(EntidadeDominio entidadeDominio) {
		
		boolean flag = false;
		
		if(((Publicacao) entidadeDominio).getConteudo().contains("Citação")) {
			System.out.println("Teste apelação válido");
			flag = true;
		}//if
		
		Resultado resultado = new Resultado();
		resultado.setStatus(flag);
		resultado.setMensagemSimples("Possibilidade de contestação identificada.");
		return resultado;
	}
		
}//IdentificaProximoPasso
