package br.com.aujuri.strategy;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;

public interface IStrategy {
	public Resultado processar(EntidadeDominio entidadeDominio);
}
