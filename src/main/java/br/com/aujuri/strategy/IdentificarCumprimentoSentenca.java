package br.com.aujuri.strategy;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Publicacao;

public class IdentificarCumprimentoSentenca implements IStrategy{

	@Override
	public Resultado processar(EntidadeDominio entidadeDominio) {
	
		boolean flag = false;
		
		if(((Publicacao) entidadeDominio).getConteudo().contains("Cumprimento")) {
			flag = true;
		}//if
		if(((Publicacao) entidadeDominio).getConteudo().contains("Cumpra-se")) {
			flag = true;
		}//if
		
		if(((Publicacao) entidadeDominio).getConteudo().contains("Cumpra-se a sentença")) {
			flag = true;
		}//if
		
		Resultado resultado = new Resultado();
		resultado.setStatus(flag);
		resultado.setMensagemSimples("Cumprimento de sentença identificado.");
		return resultado;
	}
		

}
