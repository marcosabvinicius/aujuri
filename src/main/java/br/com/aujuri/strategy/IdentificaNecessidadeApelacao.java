package br.com.aujuri.strategy;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Publicacao;

public class IdentificaNecessidadeApelacao implements IStrategy{

	@Override
	public Resultado processar(EntidadeDominio entidadeDominio) {
		
		boolean flag = false;
		
		if(((Publicacao) entidadeDominio).getConteudo().contains("Procedente")) {
			System.out.println("Teste apelação válido");
			flag = true;
		}//if
		if(((Publicacao) entidadeDominio).getConteudo().contains("Improcedente")) {
			System.out.println("Teste apelação válido");
			flag = true;
		}//if

		if(((Publicacao) entidadeDominio).getConteudo().contains("Já apreciado na sentença")) {
			System.out.println("Teste apelação válido");
			flag = true;
		}//if
		
		
		
		Resultado resultado = new Resultado();
		resultado.setStatus(flag);
		resultado.setMensagemSimples("Possibilidade de apelação identificada.");
		return resultado;
	}
}
