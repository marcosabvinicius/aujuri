package br.com.aujuri.strategy;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Publicacao;

public class IdentificaEsperaPorSentença implements IStrategy{

	@Override
	public Resultado processar(EntidadeDominio entidadeDominio) {
		boolean flag = false;
		
		if(((Publicacao) entidadeDominio).getConteudo().contains("Conclusos para Decisão")) {
			System.out.println("Teste apelação válido");
			flag = true;
		}//if
		
		Resultado resultado = new Resultado();
		resultado.setStatus(flag);
		resultado.setMensagemSimples("Aguardar decisão a ser expedida.");
		return resultado;
	}

}
