package br.com.aujuri.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import br.com.aujuri.model.ClientePessoaFisica;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.command.ICommand;
import br.com.aujuri.model.Estado;
import br.com.aujuri.model.EntidadeDominio;

@Controller
@RequestMapping("/formularioCadastroCliente")
public class ClientePFController extends AbstractSpringController{
	private Resultado resultado;
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView formularioCadastroCliente(ClientePessoaFisica clientePessoaFisica){
			modelAndView = new ModelAndView("ClientePessoaFisica/formularioCadastroCliente");
			List<Estado> listaEstado = listaEstados();
			modelAndView.addObject("listaEstados", listaEstado);
			return modelAndView;
	}
	
	//Método responsável por obter a lista de estados em um servidor WEB
	private List<Estado> listaEstados() {
		List<Estado> listaEstado = new ArrayList<>();
		Estado estado = new Estado();
		estado.setNomeEstado("Acre");
		estado.setIdEstado(1);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Alagoas");
		estado.setIdEstado(2);
		listaEstado.add(estado);

		estado = new Estado();
		estado.setNomeEstado("Amapa");
		estado.setIdEstado(3);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Amazonas");
		estado.setIdEstado(4);
		listaEstado.add(estado);

		estado = new Estado();
		estado.setNomeEstado("Bahia");
		estado.setIdEstado(5);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Ceara");
		estado.setIdEstado(6);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("DF");
		estado.setIdEstado(7);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Espirito Santo");
		estado.setIdEstado(8);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Goias");
		estado.setIdEstado(9);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Maranhao");
		estado.setIdEstado(10);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Mato Grosso");
		estado.setIdEstado(11);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Mato Grosso do Sul");
		estado.setIdEstado(12);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Minas Gerais");
		estado.setIdEstado(13);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Pará");
		estado.setIdEstado(14);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Paraiba");
		estado.setIdEstado(15);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Parana");
		estado.setIdEstado(16);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Pernambuco");
		estado.setIdEstado(17);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Piaui");
		estado.setIdEstado(18);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rio de Janeiro");
		estado.setIdEstado(19);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rio Grande do Norte");
		estado.setIdEstado(20);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rio Grande do Sul");
		estado.setIdEstado(21);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rondonia");
		estado.setIdEstado(22);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Roraima");
		estado.setIdEstado(23);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Santa Catarina");
		estado.setIdEstado(24);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Sao Paulo");
		estado.setIdEstado(25);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Sergipe");
		estado.setIdEstado(26);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Tocantins");
		estado.setIdEstado(27);
		listaEstado.add(estado);
		
		return listaEstado;
	}
	
	@RequestMapping(value="/cadastrar",method = RequestMethod.POST)
	public ModelAndView cadastrar(@ModelAttribute ClientePessoaFisica clientePessoaFisica, RedirectAttributes redirectAttributes)
	{
		ICommand iCommand = commands.get("SALVAR");
		Resultado resultado = iCommand.execute(clientePessoaFisica);
		redirectAttributes.addFlashAttribute("mensagemSimples", resultado.getMensagemSimples());
		return listarClientesPF();
	}
	
	@RequestMapping(value="ClientePessoaFisica/listaClientesPF", method=RequestMethod.GET)
	public ModelAndView listarClientesPF(){
		
		modelAndView = new ModelAndView("ClientePessoaFisica/listaClientesPF");
		resultado = commands.get("CONSULTAR").execute(new ClientePessoaFisica());		
		modelAndView.addObject("listaClientePF", resultado.getEntidadeDominios());
		return modelAndView;

	}
	
	@RequestMapping(value = "/formularioAlterar/{id}", method=RequestMethod.GET)
	public ModelAndView formularioAlterar(@PathVariable Integer id)
	{
		List<Estado> listaEstado = listaEstados();
		modelAndView = new ModelAndView("ClientePessoaFisica/editar");
		ClientePessoaFisica clientePessoaFisica = new ClientePessoaFisica();
		clientePessoaFisica.setIdPessoa(id);
		resultado = commands.get("CONSULTAR").execute(clientePessoaFisica);
		clientePessoaFisica = (ClientePessoaFisica) resultado.getEntidadeDominios().get(0);
		modelAndView.addObject("clientePessoaFisica", clientePessoaFisica);
		modelAndView.addObject("listaEstados", listaEstado);
		return modelAndView;
		
	}
	
	@RequestMapping(value="/detalhe",params = "alterar",method=RequestMethod.POST)
	public ModelAndView alterar(ClientePessoaFisica clientePessoaFisica)
	{
		resultado = commands.get("ALTERAR").execute(clientePessoaFisica);
		return listarClientesPF();
	}
		
	@RequestMapping(value="/detalhe", params= "excluir", method = RequestMethod.POST)
	public ModelAndView excluir(ClientePessoaFisica clientePessoaFisica)
	{
		resultado = commands.get("EXCLUIR").execute(clientePessoaFisica);
		return listarClientesPF();
	}
	
	@RequestMapping("/detalheCliente/{nome}/{chave}")
	@ResponseBody
	public List<EntidadeDominio> detalheCliente(@PathVariable("nome") String valor, @PathVariable("chave") String chave)
	{
		ClientePessoaFisica clientePessoaFisica  = new ClientePessoaFisica();;
		if(chave.equals("nome"))
			clientePessoaFisica.setNome(valor);
		else if(chave.equals("cpf"))
			clientePessoaFisica.setCpf(valor);
		else if(chave.equals("rg"))
			clientePessoaFisica.setRg(valor);
		resultado = commands.get("CONSULTAR").execute(clientePessoaFisica);
		return resultado.getEntidadeDominios();
	}
	
}
