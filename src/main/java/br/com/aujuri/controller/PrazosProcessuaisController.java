package br.com.aujuri.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.Funcionario;
import br.com.aujuri.model.Prazos;

@Controller
@RequestMapping("/listaPrazos")
public class PrazosProcessuaisController extends AbstractSpringController{
	
	private Resultado resultado;
	
	@RequestMapping("/listaPrazosCiveis")
	public ModelAndView listarPrazosCiveis(){
		modelAndView = new ModelAndView("prazos/listaPrazos");
		resultado = commands.get("CONSULTAR").execute(new Prazos());
		modelAndView.addObject("listaPrazos", resultado.getEntidadeDominios());
		System.out.println(modelAndView);
		return modelAndView;
	}
	
}
