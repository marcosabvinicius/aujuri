package br.com.aujuri.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.command.ICommand;
import br.com.aujuri.model.ClientePessoaFisica;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.model.Funcionario;
import br.com.aujuri.model.Movimentacoes;
import br.com.aujuri.model.PecaProcessual;
import br.com.aujuri.model.Pessoa;
import br.com.aujuri.model.Processo;
import br.com.aujuri.model.Publicacao;
import br.com.aujuri.strategy.IStrategy;
import br.com.aujuri.strategy.IdentificaEsperaPorSentença;
import br.com.aujuri.strategy.IdentificaNecessidadeAgravo;
import br.com.aujuri.strategy.IdentificaNecessidadeApelacao;
import br.com.aujuri.strategy.IdentificaNecessidadeContestacao;
import br.com.aujuri.strategy.IdentificaNecessidadeEmbargos;
import br.com.aujuri.strategy.IdentificarCumprimentoSentenca;
import br.com.aujuri.util.BuscarProcessoTJ;
import br.com.aujuri.util.FileSaver;

@Controller
@RequestMapping("/Processo")
public class ProcessoController extends AbstractSpringController {

	@Autowired 
	private FileSaver fileSaver;
	
	@RequestMapping("/Cliente")
	public ModelAndView cadastroProcessoConsultaCliente()
	{
		modelAndView = new ModelAndView("Processo/consultarCliente");
		return modelAndView;
	}
	
	@RequestMapping(value = "/CadastroProcesso/{idPessoa}", method = RequestMethod.GET)
	public ModelAndView cadastroProcesso(@PathVariable Integer idPessoa)
	{
		ClientePessoaFisica clientePessoaFisica = new ClientePessoaFisica();
		clientePessoaFisica.setIdPessoa(idPessoa);
		modelAndView = new ModelAndView("Processo/cadastroProcesso");
		modelAndView.addObject("cliente", clientePessoaFisica);
		return modelAndView;
	}
	
	@RequestMapping(value = "/cadastrar", method=RequestMethod.POST)
	public ModelAndView cadastrar(ClientePessoaFisica clientePessoaFisica)
	{
		commands.get("ALTERAR").execute(clientePessoaFisica);
		return listarPorCliente(clientePessoaFisica.getIdPessoa());
	}
	
	@RequestMapping(value="/listarPorCliente/{id}")
	public ModelAndView listarPorCliente(@PathVariable Integer id)
	{
		ClientePessoaFisica entidadeDominio = new ClientePessoaFisica();
		entidadeDominio.setIdPessoa(id);
		modelAndView = new ModelAndView("Processo/lista");
		Resultado resultado = commands.get("CONSULTAR").execute(entidadeDominio);
		List<Processo> processos = ((ClientePessoaFisica) resultado.getEntidadeDominios().get(0)).getProcessos();
		System.out.println(processos.get(0).getIdProcesso());
		modelAndView.addObject("listaProcesso", processos);
		return modelAndView;
	}
		
	//dia 02/04/18 adicionado método para listar todos os processos
	@RequestMapping(value = "/consultarProcessos", method = RequestMethod.GET)
	public ModelAndView listarProcessos()
	{

		modelAndView = new ModelAndView("Processo/lista");
		Resultado resultado = commands.get("CONSULTAR").execute(new Processo());
		modelAndView.addObject("listaProcesso", resultado.getEntidadeDominios());
		return modelAndView;
	}//listarProcessos
	
	
	@RequestMapping("/listarPublicacoes")
	public ModelAndView listarPublicacoes(String numeroProcesso, RedirectAttributes redirectAttributes)
	{
		System.out.println("Número do processo " + numeroProcesso);
		
		modelAndView = new ModelAndView("Processo/listaPublicacao");
			List<Publicacao> publicacoes;
			try {
				publicacoes = BuscarProcessoTJ.obterPublicacoes(numeroProcesso);
				Processo processo = new Processo();
				processo.setNumeroProcesso(numeroProcesso);
				Resultado resultado = commands.get("CONSULTAR").execute(processo);
				processo = ((Processo) resultado.getEntidadeDominios().get(0));
				processo.setPublicoes(publicacoes);
				resultado = commands.get("ALTERAR").execute(processo);
				
				List<IStrategy> lista = new ArrayList<>();
				lista.add(new IdentificaEsperaPorSentença());
				lista.add(new IdentificaNecessidadeAgravo());
				lista.add(new IdentificaNecessidadeApelacao());
				lista.add(new IdentificaNecessidadeContestacao());
				lista.add(new IdentificaNecessidadeEmbargos());
				lista.add(new IdentificarCumprimentoSentenca());
				
				Resultado resultadoIdentifica = new Resultado();
				resultadoIdentifica.setMensagemSimples(new String("Não há sugestões"));
				Resultado resultadoAux;
				for(int i=publicacoes.size() - 1; i>=0;i--)
				{
					for(IStrategy s: lista)
					{
						resultadoAux = s.processar(publicacoes.get(i));
						if(resultadoAux.isStatus())
							resultadoIdentifica = resultadoAux;
					}
				}
				
				modelAndView.addObject("sugestao",resultadoIdentifica.getMensagemSimples());
				modelAndView.addObject("listaPublicacao", publicacoes);
			} catch (KeyManagementException | FailingHttpStatusCodeException | NoSuchAlgorithmException | IOException
					| ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		return modelAndView;
	}//listarPublicacoes
	
	@RequestMapping("/listarDocumentos/{numeroProcesso}")
	public ModelAndView listarPecasProcessuais(@PathVariable String numeroProcesso, RedirectAttributes redirectAttributes, Processo processo) 
	{
		System.out.println("Acessando lista de documentos referentes a processos");
		modelAndView = new ModelAndView("Processo/listaDocumentos");
		processo.setNumeroProcesso(numeroProcesso);
		System.out.println(numeroProcesso);
		Resultado resultado = commands.get("CONSULTAR").execute(processo);
		List<PecaProcessual> pecas =  ((Processo) resultado.getEntidadeDominios().get(0)).getPecas();
		System.out.println(pecas.get(0));
		modelAndView.addObject("listaDocumento", pecas);
		return modelAndView;
	}//listarPecasProcessuais
	
	
	@RequestMapping("/cadastroDocumentos/{numeroProcesso}")
	public ModelAndView formularioCadastroDocumentos(@PathVariable String numeroProcesso, RedirectAttributes redirectAttributes)
	{
		Processo processo = new Processo();
		processo.setNumeroProcesso(numeroProcesso);
		Resultado resultado = commands.get("CONSULTAR").execute(processo);
		processo = (Processo) resultado.getEntidadeDominios().get(0);
		System.out.println("Acessando formulario de cadastro de documentos");
		modelAndView = new ModelAndView("Processo/cadastroDocumentos");
		modelAndView.addObject("processo",processo);
		return modelAndView;
	}//formularioCadastroDocumentos
	
	
	@RequestMapping(value = "gravarArquivo",method=RequestMethod.POST)
	public ModelAndView gravarArquivo(MultipartFile caminhoArquivo, @Valid Processo processo, BindingResult result){
		
		PecaProcessual pecaProcessual = processo.getPecas().get(0);
		List<PecaProcessual> pecas = new ArrayList<>();
		pecas.add(pecaProcessual);
		Resultado resultado = commands.get("CONSULTAR").execute(processo);
		processo = ((Processo) resultado.getEntidadeDominios().get(0));
		processo.setPecas(pecas);
		String path =  fileSaver.write("C:\\Users\\Marcos Vinicius\\Desktop\\arquivos-processos", caminhoArquivo);
		
		processo.getPecas().get(0).setCaminhoArquivo(path);
		resultado = commands.get("ALTERAR").execute(processo);
        
        return new ModelAndView("redirect:consultarProcessos");
 
	}
	
	@RequestMapping("/buscarProcessoPorCliente")
	public ModelAndView buscarProcessoPorCliente(ClientePessoaFisica clientePessoaFisica)
	{
		modelAndView = new ModelAndView("Processo/buscarProcessoCliente");
		return modelAndView;
	}

	@RequestMapping("/listarProcessoPorCliente")
	public ModelAndView listarProcessoPorCliente(ClientePessoaFisica clientePessoaFisica)
	{
		modelAndView = new ModelAndView("Processo/listarProcessoCliente");
		Resultado resultado = commands.get("CONSULTAR").execute(clientePessoaFisica);
		ClientePessoaFisica cliente = (ClientePessoaFisica) resultado.getEntidadeDominios().get(0);
		List<Processo> processos = cliente.getProcessos();
		modelAndView.addObject("processos", processos);
		return modelAndView;
	}
	
}
