package br.com.aujuri.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {

	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView index ()
	{
		return new ModelAndView("index");
	}
	
	@RequestMapping(method=RequestMethod.GET,value="logout")
	public ModelAndView logout()
	{
		return login();
	}
	
	@RequestMapping("login")
	public ModelAndView login()
	{
		return new ModelAndView("login");
	}
}
