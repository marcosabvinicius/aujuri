package br.com.aujuri.controller;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.command.ICommand;
import br.com.aujuri.model.Estado;
import br.com.aujuri.model.Funcionario;
import br.com.aujuri.model.Role;
import br.com.aujuri.model.TipoFuncionario;

@Controller
@RequestMapping("/formularioFuncionario")
public class FuncionarioController extends AbstractSpringController{
	private Resultado resultado;

	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView formularioCadastro(Funcionario funcionario	)
	{
		modelAndView = new ModelAndView("Funcionario/formularioCadastro");
		List<Estado> listaEstado = listaEstados();
		modelAndView.addObject("cargos", TipoFuncionario.values());
		modelAndView.addObject("listaEstados", listaEstado);
		return modelAndView;
	}
	
	
	//Método responsável por obter a lista de estados em um servidor WEB
	private List<Estado> listaEstados() {
		List<Estado> listaEstado = new ArrayList<>();
		Estado estado = new Estado();
		estado.setNomeEstado("Acre");
		estado.setIdEstado(1);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Alagoas");
		estado.setIdEstado(2);
		listaEstado.add(estado);

		estado = new Estado();
		estado.setNomeEstado("Amapa");
		estado.setIdEstado(3);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Amazonas");
		estado.setIdEstado(4);
		listaEstado.add(estado);

		estado = new Estado();
		estado.setNomeEstado("Bahia");
		estado.setIdEstado(5);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Ceara");
		estado.setIdEstado(6);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("DF");
		estado.setIdEstado(7);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Espirito Santo");
		estado.setIdEstado(8);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Goias");
		estado.setIdEstado(9);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Maranhao");
		estado.setIdEstado(10);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Mato Grosso");
		estado.setIdEstado(11);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Mato Grosso do Sul");
		estado.setIdEstado(12);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Minas Gerais");
		estado.setIdEstado(13);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Pará");
		estado.setIdEstado(14);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Paraiba");
		estado.setIdEstado(15);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Parana");
		estado.setIdEstado(16);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Pernambuco");
		estado.setIdEstado(17);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Piaui");
		estado.setIdEstado(18);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rio de Janeiro");
		estado.setIdEstado(19);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rio Grande do Norte");
		estado.setIdEstado(20);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rio Grande do Sul");
		estado.setIdEstado(21);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rondonia");
		estado.setIdEstado(22);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Roraima");
		estado.setIdEstado(23);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Santa Catarina");
		estado.setIdEstado(24);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Sao Paulo");
		estado.setIdEstado(25);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Sergipe");
		estado.setIdEstado(26);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Tocantins");
		estado.setIdEstado(27);
		listaEstado.add(estado);
		
		return listaEstado;
	}
	
	
	@RequestMapping(value="/cadastrar",method = RequestMethod.POST)
	public ModelAndView cadastrar(@ModelAttribute Funcionario funcionario, RedirectAttributes redirectAttributes)
	{
		Role admin = new Role();
		admin.setNome("ROLE_ADMIN");
		Role advogado = new Role();
		advogado.setNome("ROLE_ADVOGADO");
		Role estagiario = new Role();
		estagiario.setNome("ROLE_ESTAG");
		List<Role> direitos = new ArrayList<>();
		if(funcionario.getTipoFuncionario() == TipoFuncionario.ADMIN)
			direitos.add(admin);
		else if(funcionario.getTipoFuncionario() == TipoFuncionario.ADVOGADO)
			direitos.add(advogado);
		else if(funcionario.getTipoFuncionario() == TipoFuncionario.ESTAGIARIO)
			direitos.add(estagiario);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		funcionario.getLogin().setSenha(encoder.encode(funcionario.getLogin().getSenha()));
		funcionario.getLogin().setRole(direitos);
		ICommand iCommand = commands.get("SALVAR");
		Resultado resultado = iCommand.execute(funcionario);
		redirectAttributes.addFlashAttribute("mensagemSimples", resultado.getMensagemSimples());
		return listarFuncionario();
	}
	
	@RequestMapping("/listaFuncionarios")
	public ModelAndView listarFuncionario(){
		modelAndView = new ModelAndView("Funcionario/listaFuncionarios");
		resultado = commands.get("CONSULTAR").execute(new Funcionario());
		modelAndView.addObject("listaFuncionario", resultado.getEntidadeDominios());
		return modelAndView;
	}
	
	@RequestMapping(value = "/formularioAlterar/{id}", method=RequestMethod.GET)
	public ModelAndView formularioAlterar(@PathVariable Integer id)
	{
		List<Estado> listaEstado = listaEstados();
		modelAndView = new ModelAndView("Funcionario/editar");
		Funcionario funcionario = new Funcionario();
		funcionario.setIdPessoa(id);
		resultado = commands.get("CONSULTAR").execute(funcionario);
		funcionario = (Funcionario) resultado.getEntidadeDominios().get(0);
		modelAndView.addObject("funcionario", funcionario);
		modelAndView.addObject("cargos", TipoFuncionario.values());
		modelAndView.addObject("listaEstados", listaEstado);
		return modelAndView;
	}
	
	@RequestMapping(value="/detalhe",params = "alterar",method=RequestMethod.POST)
	public ModelAndView alterar(Funcionario funcionario)
	{
		resultado = commands.get("ALTERAR").execute(funcionario);
		return listarFuncionario();
	}
	
	@RequestMapping(value="/detalhe", params= "excluir", method = RequestMethod.POST)
	public ModelAndView excluir(Funcionario funcionario)
	{
		resultado = commands.get("EXCLUIR").execute(funcionario);
		return listarFuncionario();
	}
	
}
