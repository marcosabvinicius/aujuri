package br.com.aujuri.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.aujuri.model.ClientePessoaFisica;


@Controller
@RequestMapping("/relatorios")
public class ReportController extends AbstractSpringController{
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView formularioCadastroCliente(ClientePessoaFisica clientePessoaFisica){
			modelAndView = new ModelAndView("ClientePessoaFisica/formularioCadastroCliente");
			return modelAndView;
	}

}
