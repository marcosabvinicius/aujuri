package br.com.aujuri.controller;


import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.servlet.ModelAndView;
import br.com.aujuri.command.ICommand;
import br.com.aujuri.model.EntidadeDominio;

public abstract class AbstractSpringController {
	//Todas as classes de controller deverão possuir modelandView, responsável pela comunicação do model com a controller, e a view com a controller
	protected ModelAndView modelAndView; 
	protected EntidadeDominio entidadeDominio;
	//Mapa de commands que deverá ser utilizado por todas as classes. Se não fosse injetado, não seria possível para o Spring manter o controle
	@Resource
	protected Map<String,ICommand> commands;
}
