package br.com.aujuri.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.Atividade;
import br.com.aujuri.model.EntidadeDominio;
import br.com.aujuri.util.ListaAtividadesPreco;

@Controller
@RequestMapping("/atividade")
public class AtividadeController extends AbstractSpringController{

	@RequestMapping("/cadastrar")
	public ModelAndView cadastrar()
	{
		List<Atividade> atividades = ListaAtividadesPreco.obterListaAtividade();
		for(Atividade a:atividades)
			commands.get("SALVAR").execute(a);
		return null;
	}
	
	@RequestMapping(value = "/cadastrarPreco/{id}", method=RequestMethod.GET)
	public ModelAndView cadastrarPreco(@PathVariable Integer id, Atividade atividade)
	{
		modelAndView = new ModelAndView("preco/editar");
		Atividade atividade2 = new Atividade();
		atividade2.setId(id);
		Resultado resultado = commands.get("CONSULTAR").execute(atividade2);
		modelAndView.addObject("resultadoAtividade", resultado.getEntidadeDominios().get(0));
		return modelAndView;
	}
	
	@RequestMapping("/gravar")
	public ModelAndView gravarPreco(@ModelAttribute Atividade atividade, RedirectAttributes attributes)
	{
		Resultado resultado = commands.get("CONSULTAR").execute(atividade);
		Atividade atividadeAux = (Atividade) resultado.getEntidadeDominios().get(0);
		atividade.setDescricao(atividadeAux.getDescricao());
		resultado = commands.get("ALTERAR").execute(atividade);
		attributes.addFlashAttribute("mensagem",resultado.getMensagemSimples());
		return listarPrecos();
	}
	
	@RequestMapping("/listar")
	public ModelAndView listarPrecos()
	{
		ModelAndView modelAndView = new ModelAndView("preco/consulta");
		Resultado resultado = commands.get("CONSULTAR").execute(new Atividade());
		modelAndView.addObject("listaAtividade", resultado.getEntidadeDominios());
		return modelAndView;
	}
		
}
