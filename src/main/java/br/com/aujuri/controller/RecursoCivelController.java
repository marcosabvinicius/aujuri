package br.com.aujuri.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.aujuri.application.Resultado;
import br.com.aujuri.model.Recurso;

@Controller
@RequestMapping("/listaRecursos")
public class RecursoCivelController extends AbstractSpringController{
	
	private Resultado resultado;
	
	@RequestMapping("/listaRecursosCiveis")
	public ModelAndView listarRecursosCiveis(){
		modelAndView = new ModelAndView("recursos/listaRecursos");
		resultado = commands.get("CONSULTAR").execute(new Recurso());
		modelAndView.addObject("listaRecursos", resultado.getEntidadeDominios());
		System.out.println(modelAndView);
		return modelAndView;
	}
	
}
