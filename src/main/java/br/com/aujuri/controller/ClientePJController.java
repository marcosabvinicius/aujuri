package br.com.aujuri.controller;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import br.com.aujuri.application.Resultado;
import br.com.aujuri.command.ICommand;
import br.com.aujuri.model.ClientePessoaJuridica;
import br.com.aujuri.model.Estado;

@Controller
@RequestMapping("/formularioCadastroClientePJ")
public class ClientePJController extends  AbstractSpringController {
	private Resultado resultado;
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView formularioCadastroClientePJ(ClientePessoaJuridica clientePessoaJuridica){
		modelAndView = new ModelAndView("ClientePessoaJuridica/formularioCadastroClientePJ");
		List<Estado> listaEstado = listaEstados();
		modelAndView.addObject("listaEstados", listaEstado);
		return modelAndView;
	}
	
	//Método responsável por obter a lista de estados em um servidor WEB
	private List<Estado> listaEstados() {
		List<Estado> listaEstado = new ArrayList<>();
		Estado estado = new Estado();
		estado.setNomeEstado("Acre");
		estado.setIdEstado(1);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Alagoas");
		estado.setIdEstado(2);
		listaEstado.add(estado);

		estado = new Estado();
		estado.setNomeEstado("Amapa");
		estado.setIdEstado(3);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Amazonas");
		estado.setIdEstado(4);
		listaEstado.add(estado);

		estado = new Estado();
		estado.setNomeEstado("Bahia");
		estado.setIdEstado(5);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Ceara");
		estado.setIdEstado(6);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("DF");
		estado.setIdEstado(7);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Espirito Santo");
		estado.setIdEstado(8);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Goias");
		estado.setIdEstado(9);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Maranhao");
		estado.setIdEstado(10);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Mato Grosso");
		estado.setIdEstado(11);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Mato Grosso do Sul");
		estado.setIdEstado(12);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Minas Gerais");
		estado.setIdEstado(13);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Pará");
		estado.setIdEstado(14);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Paraiba");
		estado.setIdEstado(15);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Parana");
		estado.setIdEstado(16);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Pernambuco");
		estado.setIdEstado(17);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Piaui");
		estado.setIdEstado(18);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rio de Janeiro");
		estado.setIdEstado(19);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rio Grande do Norte");
		estado.setIdEstado(20);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rio Grande do Sul");
		estado.setIdEstado(21);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Rondonia");
		estado.setIdEstado(22);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Roraima");
		estado.setIdEstado(23);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Santa Catarina");
		estado.setIdEstado(24);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Sao Paulo");
		estado.setIdEstado(25);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Sergipe");
		estado.setIdEstado(26);
		listaEstado.add(estado);
		
		estado = new Estado();
		estado.setNomeEstado("Tocantins");
		estado.setIdEstado(27);
		listaEstado.add(estado);
		
		return listaEstado;
	}
	
	
	
	
	@RequestMapping(value="/cadastrar",method = RequestMethod.POST)
	public ModelAndView cadastrar(@ModelAttribute ClientePessoaJuridica clientePessoaJuridica, RedirectAttributes redirectAttributes)
	{
		ICommand iCommand = commands.get("SALVAR");
		Resultado resultado = iCommand.execute(clientePessoaJuridica);
		redirectAttributes.addFlashAttribute("mensagemSimples", resultado.getMensagemSimples());
		return listarClientesPJ();
	}
	
	@RequestMapping(value="ClientePessoaJuridica/listaClientesPJ", method=RequestMethod.GET)
	public ModelAndView listarClientesPJ(){
		
		modelAndView = new ModelAndView("ClientePessoaJuridica/listaClientesPJ");
		resultado = commands.get("CONSULTAR").execute(new ClientePessoaJuridica());
		modelAndView.addObject("listaClientePJ", resultado.getEntidadeDominios());
		return modelAndView;

	}
	
	@RequestMapping(value = "/formularioAlterar/{id}", method=RequestMethod.GET)
	public ModelAndView formularioAlterar(@PathVariable Integer id)
	{
		List<Estado> listaEstado = listaEstados();
		modelAndView = new ModelAndView("ClientePessoaJuridica/editar");
		ClientePessoaJuridica clientePessoaJuridica = new ClientePessoaJuridica();
		clientePessoaJuridica.setIdPessoa(id);
		resultado = commands.get("CONSULTAR").execute(clientePessoaJuridica);
		clientePessoaJuridica= (ClientePessoaJuridica) resultado.getEntidadeDominios().get(0);
		modelAndView.addObject("clientePessoaJuridica", clientePessoaJuridica);
		modelAndView.addObject("listaEstados", listaEstado);
		return modelAndView;
		
	}
	
	@RequestMapping(value="/detalhe",params = "alterar",method=RequestMethod.POST)
	public ModelAndView alterar(ClientePessoaJuridica clientePessoaJuridica)
	{
		resultado = commands.get("ALTERAR").execute(clientePessoaJuridica);
		return listarClientesPJ();
	}
		
	@RequestMapping(value="/detalhe", params= "excluir", method = RequestMethod.POST)
	public ModelAndView excluir(ClientePessoaJuridica clientePessoaJuridica)
	{
		resultado = commands.get("EXCLUIR").execute(clientePessoaJuridica);
		return listarClientesPJ();
	}
	
}
