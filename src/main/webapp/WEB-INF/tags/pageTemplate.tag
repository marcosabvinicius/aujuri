<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="titulo" required="true" %>
<%@ attribute name="extraScripts" fragment="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE>
<html ng-app="aujuri">
	<head>
		<meta charset="utf-8">
		<title>${titulo} - AUJURI</title>
		<link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/bootstrap.min.css"/>>
		<link href=<c:url value="/resources/font-awesome/css/font-awesome.css"/> rel="stylesheet">
	    <!-- Page-Level Plugin CSS - Dashboard -->
	    <link href=<c:url value="/resources/css/plugins/morris/morris-0.4.3.min.css"/> rel="stylesheet">
	    <link href=<c:url value="/resources/css/plugins/timeline/timeline.css"/> rel="stylesheet">
	    <!-- SB Admin CSS - Include with every page -->
	    <link href=<c:url value="/resources/css/sb-admin.css"/> rel="stylesheet">
		<link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/navbar-fixed-side.css"/>>
		<script src=<c:url value="/resources/js/jquery-3.2.1.min.js"/>></script>
		<script src=<c:url value="/resources/js/bootstrap.min.js"/>></script>
	    <script src=<c:url value="/resources/js/plugins/metisMenu/jquery.metisMenu.js"/>></script>
	    <!-- Page-Level Plugin Scripts - Dashboard -->
	    <script src=<c:url value="/resources/js/plugins/morris/raphael-2.1.0.min.js"/>></script>
	    <script src=<c:url value="/resources/js/plugins/morris/morris.js"/>></script>
	    <!-- SB Admin Scripts - Include with every page -->
	    <script src=<c:url value="/resources/js/sb-admin.js"/>></script>	
	    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
	    <script src=<c:url value="/resources/js/demo/dashboard-demo.js"/>></script>
	    <script src=<c:url value="/resources/js/ajax.js"/>></script>
	    <script src=<c:url value="/resources/js/lib/angular.min.js"/>></script>
	    <script src=<c:url value="/resources/js/aujuriApp.js"/>></script> 
	</head>
	<body>
		<%@include file="/WEB-INF/views/cabecalho.jsp" %>
		<jsp:doBody />
		<%@include file="/WEB-INF/views/rodape.jsp" %>		
		<jsp:invoke fragment="extraScripts" />
	</body>
</html>