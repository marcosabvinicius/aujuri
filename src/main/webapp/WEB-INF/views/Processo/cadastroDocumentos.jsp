x<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<tags:pageTemplate titulo="Cadastro de processo">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Cadastro de Documentos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
	<form:form class="form-horizontal" action="${s:mvcUrl('PC#gravarArquivo').build()}" method="POST" commandName="processo" enctype="multipart/form-data">
            <div class="form-group">
			    <label class="col-sm-3 control-label">Numero do Processo</label>
				<div class="col-sm-6">
			    	<form:input type="text" required="numeroProcesso"  class="form-control" placeholder="N�mero Processo" path="numeroProcesso" />
				</div>
			</div>
			
			<div class="form-group">
			    <label class="col-sm-3 control-label">Descri��o</label>
				<div class="col-sm-6">
			    	<form:input type="text" required="descricao" class="form-control" placeholder="Descri��o" path="pecas[0].descricao" id="descricao" />
				</div>
			</div>
	
			<div class="form-group">
			    <label class="col-sm-3 control-label">Anexar Arquivo</label>
				<div class="col-sm-6">
			    	<input type="file" name="caminhoArquivo" required="caminhoArquivo" class="form-control"  path="pecas[0].caminhoArquivo" id="caminhoArquivo"/>
				</div>
			</div>
	
			<center>
				<input type="submit" class="btn btn-primary" value="Cadastrar" id="btnCadastrar"> 
				<button type="reset" class="btn btn-danger" id="btnLimpar">Limpar</button>
			</center>
	
	</form:form>
</tags:pageTemplate>