<%@taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<tags:pageTemplate titulo="Lista de publicacao">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Lista de Publica��es</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <table class="table table-bordered table-striped">
    	<tr>
    		<th>Data de Publica��o</th>
    		<th>Conte�do</th>
    	</tr>
    	<tr>
    		<c:forEach items="${listaPublicacao}" var="publicacao">
    			<tr>
	    			<fmt:formatDate value="${publicacao.dataPublicacao.time}" pattern="dd/MM/yyyy" var="data"/>
	    			<td>${data}</td>
	    			<td>${publicacao.conteudo}</td>
    			</tr>
    		</c:forEach>
    	</tr>
    </table>
    
    <table class="table table-bordered table-striped">
    	<tr>
    		<th>Sugest�es</th>
    	</tr>
    	<tr>
    		
    			<tr>
	    			
	    			<td>${sugestao}</td>
	    		</tr>
    	    	</tr>
    </table>
    
</tags:pageTemplate>