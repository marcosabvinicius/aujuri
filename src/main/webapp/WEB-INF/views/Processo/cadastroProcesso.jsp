x<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<tags:pageTemplate titulo="Cadastro de processo">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Cadastro de processo</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
	<form:form method="post" commandName="cliente" action="${s:mvcUrl('PC#cadastrar').build()}" >
		<form:hidden path="idPessoa"/>
		<label>N�mero do processo</label>
		<form:input path="processos[0].numeroProcesso"/>
		<label>Parte contr�ria</label>
		<form:input path="processos[0].parteContraria"/>
		<input type="submit" value="Salvar"/>
	</form:form>
</tags:pageTemplate>