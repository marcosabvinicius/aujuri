<%@taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<tags:pageTemplate titulo="Listar Processos por Cliente">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Lista de Processos por Cliente</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <table border='1' class="table table-bordered table-striped">
		<tr>
			<th>ID</th>
			<th>N�mero do processo</th>
			<th>Parte Contr�ria</th>
			<th>Status</th>
			<th>Listar publica��es</th>
			<th>Documentos</th>
			<th>Cadastrar</th>
		</tr>
		<tr>
			<c:forEach items="${processos}" var="processo">
				<tr>
					<td>${processo.idProcesso}</td>
					<td>${processo.numeroProcesso}</td>
					<td>${processo.parteContraria}</td>
					<td>${processo.statusProcesso}</td>
					<td><form:form method="get" action="${s:mvcUrl('PC#listarPublicacoes').build()}"><input type="hidden" name="numeroProcesso" value="${processo.numeroProcesso}"/><button type="submit" class="btn-link">Buscar publica��o</button></form:form></td>
					<td><a href="${s:mvcUrl('PC#listarPecasProcessuais').arg(0, processo.numeroProcesso).build()}" id="listarPecasProcessuais">Consultar Documentos</a></td></td>
					<td><a href="${s:mvcUrl('PC#formularioCadastroDocumentos').arg(0, processo.numeroProcesso).build()}" id="formularioCadastroDocumentos">Novo Documento</a></td>
				</tr>
			</c:forEach>
		</tr>
	</table>
    
    <table class="table table-bordered table-striped">
    	<tr>
    		<th>Sugest�es</th>
    	</tr>
    	<tr>
    		
    			<tr>
	    			
	    			<td>${sugestao}</td>
	    		</tr>
    	    	</tr>
    </table>
    
</tags:pageTemplate>