<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<tags:pageTemplate titulo="Cliente do processo">
<script src=<c:url value="/resources/js/controller/ProcessoController.js"/>></script>
 <div id="page-wrapper" ng-controller="processoController">
    <div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Consulta de cliente</h1>
	    </div>
	    <!-- /.col-lg-12 -->
    </div>
	<label>Pesquisar: </label>
	<input type="text" ng-model="pesquisa.valorPesquisa"/>
	<input type="radio" name="tipoInformacao" ng-model="pesquisa.criterioPesquisa" value="nome"/><label>Nome</label>
	<input type="radio" name="tipoInformacao" ng-model="pesquisa.criterioPesquisa" value="cpf"/><label>CPF</label>
	<input type="radio" name="tipoInformacao" ng-model="pesquisa.criterioPesquisa" value="rg"/><label>RG</label>
	<input type="button" value="Pesquisar" ng-click="pesquisarCliente(pesquisa)"/>
	
	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>CPF</th>
			<th>RG</th>
		</tr>
		<tr ng-repeat="cliente in clientes">
			<td>{{cliente.idPessoa}}</td/>
			<td><a href="${s:mvcUrl('PC#cadastroProcesso').build()}/{{cliente.idPessoa}}">{{cliente.nome}}</a></td>
			<td>{{cliente.cpf}}</td>
			<td>{{cliente.rg}}</td>
		</tr>
	</table>
</div>
</tags:pageTemplate>