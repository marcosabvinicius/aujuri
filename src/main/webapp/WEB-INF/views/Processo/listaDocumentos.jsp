<%@taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<tags:pageTemplate titulo="Lista de Documentos">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Lista de Documentos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <table class="table table-bordered table-striped">
    	<tr>
    		<th>Descri��o</th>
    		<th>Caminho do Arquivo</th>
    		<th>Excluir</th>
    	</tr>
    	<tr>
    		<c:forEach items="${listaDocumento}" var="pecas">
    			<tr>
	    			<td>${pecas.descricao}</td>
	    			<td>${pecas.caminhoArquivo}</td>
	    			<td><a>Excluir Documento</a></td>
    			</tr>
    		</c:forEach>
    	</tr>
    </table>
    
   
</tags:pageTemplate>