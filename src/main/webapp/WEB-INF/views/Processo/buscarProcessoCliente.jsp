<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<tags:pageTemplate titulo="Buscar processo por cliente">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Buscar processo por cliente</h1>
        </div>
        <!-- /.col-lg-12 -->
        <form:form commandName="clientePessoaFisica" action="${s:mvcUrl('PC#listarProcessoPorCliente').build()}">
        	<form:label path="cpf">CPF:</form:label>
        	<form:input path="cpf"/>
        	<input type="submit" value="Buscar">
        </form:form>
    </div>
</tags:pageTemplate>