<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=${s:mvcUrl('HC#index').build()}>AUJURI</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href=${s:mvcUrl('HC#index').build()}><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw" ></i> Relat�rios</a>
                        </li>
                        <li>
                            <a href="#"><i class="glyphicon glyphicon-user" id="cliente"></i> Cliente<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" id="pessoaFisica">Pessoa F�sica <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Gerar or�amento</a>
                                        </li>
                                        <li>
                                            <a href=${s:mvcUrl('CPFC#formularioCadastroCliente').build()} id="cadastrarClientePF">Cadastrar</a>
                                        </li>
                                        <li>
                                            <a href=${s:mvcUrl('CPFC#listarClientesPF').build()} id="consultarClientePF">Consultar</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li>
                                    <a href="#" id="pessoaJuridica">Pessoa Jur�dica <span class="fa arrow"></span></a>
	                                  <ul class="nav nav-third-level">
	                                        <li>
	                                            <a href="#">Gerar or�amento</a>
	                                        </li>
	                                        <li>
	                                            <a href=${s:mvcUrl('CPJC#formularioCadastroClientePJ').build()} id="cadastrarClientePJ">Cadastrar</a>
	                                        </li>
	                                        <li>
	                                            <a href=${s:mvcUrl('CPJC#listarClientesPJ').build()} id="consultarClientePJ">Consultar</a>
	                                        </li>
	                                    <!-- /.nav-third-level -->
	                                   </ul>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        	<a href="#" id="funcionario" name="funcionario"><i class=" fa fa-usd"></i> Pre�o<span class="fa arrow"></span></a>
                        	<ul class="nav nav-second-level">
                        		<li>
	                            	<a href=${s:mvcUrl('AC#listarPrecos').build()} id="listarPreco">Consultar</a>
	                            </li>
	                        </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i>Processo<span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                        		<li>
	                            	<a href=${s:mvcUrl('PC#cadastroProcessoConsultaCliente').build()}>Cadastrar</a>
	                            </li>
	                            <li>
	                            	<a href=${s:mvcUrl('PC#listarProcessos').build()}>Consultar</a>
	                            </li>
	                          	<li>
	                            	<a href=${s:mvcUrl('PC#buscarProcessoPorCliente').build()}>Consultar por cliente</a>
	                            </li>
	                        </ul>
                        </li>
                        <li>
                        	<a href="#" id="funcionario" name="funcionario"><i class="glyphicon glyphicon-plus"></i> Funcion�rio <span class="fa arrow"></span></a>
                        	<ul class="nav nav-second-level">
                        		<li>
	                            	<a href=${s:mvcUrl('FC#formularioCadastro').build()} id="cadastrarFuncionario">Cadastrar</a>
	                            </li>
	                            <li>
	                            	<a href=${s:mvcUrl('FC#listarFuncionario').build()}>Consultar</a>
	                            </li>
	                        </ul>
                        </li>
                         <li>
                        	<a href="#"><i class="fa fa-table fa-fw"></i>Prazos Processuais<span class="fa arrow"></a>
                        	<ul class="nav nav-second-level">
                        		<li>
	                            	<a href=${s:mvcUrl('PPC#listarPrazosCiveis').build()} id="listarPrazosCiveis">Processo Civil</a>
	                            </li>
	                        </ul>
                        </li>
                        <li>
                        	<a href="#"><i class="fa fa-table fa-fw"></i>Recursos<span class="fa arrow"></a>
                        	<ul class="nav nav-second-level">
                        		<li>
	                            	<a href=${s:mvcUrl('RCC#listarRecursosCiveis').build()} id="listarRecursosCiveis">Processo Civil</a>
	                            </li>
	                        </ul>
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
    </div>