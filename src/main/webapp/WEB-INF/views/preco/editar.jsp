<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<tags:pageTemplate titulo="Cadastrar pre�o">
	    <script src=<c:url value="/resources/js/util.js"/>></script>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Editar Pre�o</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <form:form class="form-horizontal" method="post" commandName="atividade" action="${s:mvcUrl('AC#gravarPreco').build()}">
                <div class="form-group">
	            	<form:label path="descricao">Descri��o: </form:label>
	            	<form:label path="descricao">${resultadoAtividade.descricao}</form:label>
	            </div>
	            <form:hidden value="${resultadoAtividade.descricao}" path="descricao"/>
	            <form:hidden path="id" value="${resultadoAtividade.id}"/>
	            <div class="form-group">
	            	<form:label path="preco">Valor: </form:label>
	            	<form:input path="preco"/>
            	</div>
            	<form:hidden path="descricao"/>
				<center>
				  <input type="submit" id="btnCadastrar" name="btnCadastrar" class="btn btn-primary" value="Cadastrar"> 
				  <button type="reset" class="btn btn-danger">Limpar</button>
				</center>
            </form:form>
        </div>
</tags:pageTemplate>