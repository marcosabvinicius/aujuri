<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<tags:pageTemplate titulo="Consulta de pre�os">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Lista de Pre�os</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="table-responsive">
		<table class="table table-bordered table-striped">
    		<tr>
    			<th>ID</th>
    			<th>Descri��o</th>
    			<th>Pre�o</th>
    		</tr>
    		<c:forEach items="${listaAtividade}" var="atividade">
	    		<tr>
	
		    			<td><a href="${s:mvcUrl('AC#cadastrarPreco').arg(0, atividade.id).build()}">${atividade.id}</a></td>
		    			<td>${atividade.descricao}</td>
		    			<td>${atividade.preco}</td>
		    	</tr>
			</c:forEach>
  		</table>
	</div>
</tags:pageTemplate>