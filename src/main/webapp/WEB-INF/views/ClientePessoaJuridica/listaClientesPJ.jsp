<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<tags:pageTemplate titulo="Lista de Clientes - Pessoa F�sica">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Lista de Clientes - Pessoa Juridica</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="table-responsive">
 		<table class="table table-bordered table-striped">
    		<tr>
    			<th>ID</th>
    			<th>Raz�o Social</th>
    			<th>CNPJ</th>
    			<th>Empresa</th>
    		</tr>
    		<c:forEach items="${listaClientePJ}" var="clientePessoaJuridica">
	    		<tr>
	
		    			<td><a href="${s:mvcUrl('CPJC#formularioAlterar').arg(0, clientePessoaJuridica.idPessoa).build()}">${clientePessoaJuridica.idPessoa}</a></td>
		    			<td>${clientePessoaJuridica.razaoSocial}</td>
		    			<td>${clientePessoaJuridica.cnpj}</td>
		    			<td>${clientePessoaJuridica.empresa}</td>
		    	</tr>
			</c:forEach>
  		</table>
	</div>
</tags:pageTemplate>