<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form"  prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<tags:pageTemplate titulo="Cadastro de Usu�rio">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detalhes de Cliente</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <form:form class="form-horizontal" method="post" commandName="clientePessoaJuridica" action="/aujuri/formularioCadastroClientePJ/detalhe">
            	  <form:hidden path="idPessoa" value="${clientePessoaJuridica.idPessoa}"/>
            	  <div class="form-group">
				    <label class="col-sm-3 control-label">Raz�o Social</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="razaoSocial" name="razaoSocial" required="razaoSocial" class="form-control" placeholder="RazaoSocial" path="razaoSocial" pattern="[A-Za-z�-� ']+$" value="${clientePessoaJuridica.razaoSocial}"/>
				    </div>
				  </div>
            	  
            	   <div class="form-group">
				    <label class="col-sm-3 control-label">CNPJ</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="cnpj" name= "cnpj" required="cnpj" class="form-control" placeholder="CNPJ" path="cnpj" maxlength="14" minlength="14" pattern="[0-9]+$" value="${clientePessoaJuridica.cnpj}"/>
				    </div>
				  </div>
            	  
			       <div class="form-group">
				    <label class="col-sm-3 control-label">Pessoa para contato</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="contato" name="contato" required="contato" class="form-control" placeholder="Contato" path="contato" pattern="[A-Za-z�-� ']+$" value="${clientePessoaJuridica.contato}"/>
				    </div>
				  </div>	  
				  	<div class="form-group">
				    <label class="col-sm-3 control-label">Empresa</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="empresa" name ="empresa" required="empresa" class="form-control" placeholder="Empresa" path="empresa" pattern="[a-zA-Z 0-9]+$" value = "${clientePessoaJuridica.empresa}"/>
				    </div>
				  </div>	 
				   <div class="form-group">
				    <label class="col-sm-3 control-label">Rua</label>
				    <div class="col-sm-3">
				      <form:input type="text" id="rua" name = "rua" required="rua" class="form-control" placeholder="Rua" path="endereco.nomeRua" pattern="[A-Za-z�-� ']+$" value="${clientePessoaJuridica.endereco.nomeRua}"/>
				    </div>
				    <label class="col-sm-1 control-label">Numero</label>
				    <div class="col-sm-2">
				      <form:input type="text" id="numero" name="numero" class="form-control" required="numero" placeholder="Numero" path="endereco.numero" maxlength="5" pattern="[a-zA-Z 0-9]+$" value="${clientePessoaJuridica.endereco.numero}"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">CEP</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="cep" name="cep" class="form-control" required="cep" placeholder="CEP" path="endereco.cep" maxlength="8" minlength="8" pattern="[0-9]+$" value="${clientePessoaJuridica.endereco.cep}"/>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Bairro</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="bairro" name="bairro" required="bairro" class="form-control" placeholder="Bairro" path="endereco.bairro" pattern="[A-Za-z�-� ']+$" value="${clientePessoaJuridica.endereco.bairro}"/>
				  	</div>
				  </div>

				  <div class="form-group">
				    <label class="col-sm-3 control-label">Estado</label>
				    <div class="col-sm-6">
					  <form:select class="form-control" path="endereco.cidade.estado.nomeEstado">
					  <c:forEach items="${listaEstados}" var="estado">
				      	<form:option value="${estado.nomeEstado}">${estado.nomeEstado}</form:option>
				      </c:forEach>
				      </form:select>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Cidade</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="cidade" name="cidade" required="cidade" class="form-control" placeholder="Cidade" path="endereco.cidade.cidade" pattern="[A-Za-z�-� ']+$" value="${clientePessoaJuridica.endereco.cidade.cidade}"/>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">DDD</label>
				    <div class="col-sm-1">
				      <form:input type="text" id="ddd" name = "ddd" required="ddd" class="form-control" placeholder="DDD" path="telefones[0].ddd" pattern="[0-9]+$" maxlength="3" minlength="2" value="${clientePessoaJuridica.telefones[0].ddd}"/>
				    </div>
				    <label class="col-sm-1 control-label">Telefone</label>
				    <div class="col-sm-3">
				      <form:input type="text" id="telefone" name = "telefone" required="telefone" maxlength="9" minlength="8" pattern="[0-9]+$" data-mask="00000000" data-mask-selectonfocus="true" class="form-control" placeholder="Telefone" path="telefones[0].numeroTelefone" value="${clientePessoaJuridica.telefones[0].numeroTelefone}"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">E-mail</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="email" name="email" required="email" class="form-control" placeholder="E-mail" path="login.email" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="${clientePessoaJuridica.login.email}"/>
				  	</div>
				  </div>
				  <center>
					  <input type="submit" class="btn btn-primary" value="Alterar" name="alterar"/> 
					  <input type="submit" class="btn btn-danger" value="Excluir" name="excluir"/>
				  </center>
            </form:form>
        </div>
        
</tags:pageTemplate>