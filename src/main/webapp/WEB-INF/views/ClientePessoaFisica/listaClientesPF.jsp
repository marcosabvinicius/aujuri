<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<tags:pageTemplate titulo="Lista de Clientes - Pessoa F�sica">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Lista de Clientes - Pessoa F�sica</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="table-responsive">
 		<table class="table table-bordered table-striped">
    		<tr>
    			<th>ID</th>
    			<th>Nome</th>
    			<th>SobreNome</th>
    			<th>RG</th>
    			<th>CPF</th>
    		</tr>
    		<c:forEach items="${listaClientePF}" var="clientePessoaFisica">
	    		<tr>
	
		    			<td><a href="${s:mvcUrl('CPFC#formularioAlterar').arg(0, clientePessoaFisica.idPessoa).build()}">${clientePessoaFisica.idPessoa}</a></td>
		    			<td>${clientePessoaFisica.nome}</td>
		    			<td>${clientePessoaFisica.sobrenome}</td>
		    			<td>${clientePessoaFisica.rg}</td>
		    			<td>${clientePessoaFisica.cpf}</td>
		    	</tr>
			</c:forEach>
  		</table>
	</div>
</tags:pageTemplate>