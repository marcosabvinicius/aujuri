<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form"  prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<tags:pageTemplate titulo="Cadastro de Clientes">
		<script src=<c:url value="/resources/js/util.js"/>></script>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cadastro de Clientes</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <form:form class="form-horizontal" method="post" commandName="clientePessoaFisica" action="${s:mvcUrl('CPFC#cadastrar').build()}">
            	  <div class="form-group">
				    <label class="col-sm-3 control-label">Nome</label>
				    <div class="col-sm-6">
				      <form:input type="text" required="nome" class="form-control" placeholder="Nome" path="nome" id="nomePF" pattern="[A-Za-z�-� ']+$"/>
				    </div>
				  </div>
			      <div class="form-group">
				    <label class="col-sm-3 control-label">Sobrenome</label>
				    <div class="col-sm-6">
				      <form:input type="text" required="sobrenome" class="form-control" placeholder="Sobrenome" path="sobrenome" id="sobrenomePF" pattern="[A-Za-z�-� ']+$"/>
				    </div>
				  </div>	  
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Data de nascimento</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="dataNasc" name="dataNasc" required = "dataNascimento" class="form-control" placeholder="Exemplo: DD/MM/AAAA" path="dataNascimento" pattern="[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" min="1930-01-01" max="2014-02-18" onkeypress="mascaraData()" maxlength="10"/>
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">RG</label>
				    <div class="col-sm-6">
				      <form:input type="text" maxlength="10" minlength="8" required = "rg" class="form-control" placeholder="RG" path="rg" id="rg" pattern="[a-zA-Z0-9]+$"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">CPF</label>
				    <div class="col-sm-6">
				      <form:input type="text" maxlength="11" minlength="11" required = "cpf" class="form-control" placeholder="CPF" path="cpf" id="cpf" pattern="[0-9]+$"/>
				    </div>
				  </div>	
				  
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Rua</label>
				    <div class="col-sm-3">
				      <form:input type="text" required = "rua" class="form-control" placeholder="Rua" path="endereco.nomeRua" id="rua" pattern="[A-Za-z�-� ']+$"/>
				    </div>
				    <label class="col-sm-1 control-label">Numero</label>
				    <div class="col-sm-2">
				      <form:input type="text" required="numero" maxlength="5" class="form-control" placeholder="Numero" path="endereco.numero" id="numero" pattern="[a-zA-Z 0-9]+$"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">CEP</label>
				    <div class="col-sm-6">
				      <form:input type="text" maxlength="8" minlength="8" required = "cep" class="form-control" placeholder="CEP" path="endereco.cep" id="cep" pattern="[0-9]+$"/>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Bairro</label>
				    <div class="col-sm-6">
				      <form:input type="text" required = "bairro" class="form-control" placeholder="Bairro" path="endereco.bairro" id="bairro" pattern="[A-Za-z�-� ']+$"/>
				  	</div>
				  </div>

				  <div class="form-group">
				    <label class="col-sm-3 control-label">Estado</label>
				    <div class="col-sm-6">
					  <form:select id="formEstado" name="formEstado" class="form-control" path="endereco.cidade.estado.nomeEstado">
					  <c:forEach items="${listaEstados}" var="estado">
				      	<form:option value="${estado.nomeEstado}">${estado.nomeEstado}</form:option>
				      </c:forEach>
				      </form:select>
				  	</div>
				  </div>

				  <div class="form-group">
				    <label class="col-sm-3 control-label">Cidade</label>
				    <div class="col-sm-6">
				      <form:input type="text" required = "cidade" class="form-control" placeholder="Cidade" path="endereco.cidade.cidade" id="cidade" pattern="[A-Za-z�-� ']+$"/>
				  	</div>
				  </div>
				  				  
				  <div class="form-group">
				    <label class="col-sm-3 control-label">DDD</label>
				    <div class="col-sm-1">
				      <form:input type="text" maxlength="3" minlength="2" class="form-control" placeholder="DDD" path="telefones[0].ddd" id="ddd" pattern="[0-9]+$"/>
				    </div>
				    <label class="col-sm-1 control-label">Telefone</label>
				    <div class="col-sm-3">
				      <form:input type="text" class="form-control" required = "numeroTelefone" placeholder="Telefone" path="telefones[0].numeroTelefone" id="telefone" maxlength="9" minlength="8" pattern="[0-9]+$" data-mask="00000000" data-mask-selectonfocus="true"/>
				    </div>
				  </div>
				   
				  <div class="form-group">
				    <label class="col-sm-3 control-label">E-mail</label>
				    <div class="col-sm-6">
				      <form:input type="text" required = "email" class="form-control" placeholder="E-mail" path="login.email" id="email" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
				  	</div>
				  </div>
				  
				  <center>
					  <input type="submit" class="btn btn-primary" value="Cadastrar" id="btnCadastrar"> 
					  <button type="reset" class="btn btn-danger" id="btnLimpar">Limpar</button>
				  </center>
            </form:form>
        </div>
        
</tags:pageTemplate>