<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<tags:pageTemplate titulo="Lista de Funcionarios">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Lista de Funcion�rio</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="table-responsive">
 		<table class="table table-bordered table-striped">
    		<tr>
    			<th>ID</th>
    			<th>Nome</th>
    			<th>N�mero da OAB</th>
    			<th>Cargo</th>
    		</tr>
    		<c:forEach items="${listaFuncionario}" var="funcionario">
	    		<tr>
	
		    			<td><a href="${s:mvcUrl('FC#formularioAlterar').arg(0, funcionario.idPessoa).build()}">${funcionario.idPessoa}</a></td>
		    			<td>${funcionario.nome}</td>
		    			<td>${funcionario.numeroOAB}</td>
		    			<td>${funcionario.tipoFuncionario}</td>
	
	    		</tr>
			</c:forEach>
  		</table>
	</div>
</tags:pageTemplate>