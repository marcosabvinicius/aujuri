<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form"  prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<tags:pageTemplate titulo="Cadastro de Usu�rio">
       	<script src=<c:url value="/resources/js/util.js"/>></script>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detalhes de Funcion�rio</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <form:form class="form-horizontal" method="post" commandName="funcionario" action="/aujuri/formularioFuncionario/detalhe">
            	  <form:hidden path="idPessoa" value="${funcionario.idPessoa}"/>
            	  <div class="form-group">
				    <label class="col-sm-3 control-label">Nome</label>
				    <div class="col-sm-6">
				      <form:input type="text" required="nome" class="form-control" placeholder="Nome" path="nome" id="nomePF" pattern="[A-Za-z�-� ']+$" value="${funcionario.nome}"/>
				    </div>
				  </div>
			      <div class="form-group">
				    <label class="col-sm-3 control-label">Sobrenome</label>
				    <div class="col-sm-6">
				      <form:input type="text" required="sobrenome" class="form-control" placeholder="Sobrenome" path="sobrenome" id="sobrenomePF" pattern="[A-Za-z�-� ']+$" value="${funcionario.sobrenome}"/>
				    </div>
				  </div>	  
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Data de nascimento</label>
				    <div class="col-sm-6">
				      <fmt:formatDate value="${funcionario.dataNascimento.time}" pattern="dd/MM/YYYY" var="dataNascimento"/>
				      <form:input type="text" class="form-control" placeholder="Data de Nascimento" path="dataNascimento" value="${dataNascimento}" onkeypress="mascaraData()" maxlength="10"/>
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">RG</label>
				    <div class="col-sm-6">
				      <form:input type="text" maxlength="10" minlength="8" required = "rg" class="form-control" placeholder="RG" path="rg" id="rg" pattern="[a-zA-Z0-9]+$" value="${funcionario.rg}"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">CPF</label>
				    <div class="col-sm-6">
				      <form:input type="text" maxlength="11" minlength="11" required = "cpf" class="form-control" placeholder="CPF" path="cpf" id="cpf" pattern="[0-9]+$" value="${funcionario.cpf}"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Registro OAB</label>
				    <div class="col-sm-6">
				      <form:input type="text" class="form-control" placeholder="Registro OAB" path="numeroOAB" value="${funcionario.numeroOAB}"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Cargo</label>
				    <div class="col-sm-6">
				      <form:select class="form-control" path="tipoFuncionario">
				      	<c:forEach items="${cargos}" var="tipoCargo">
				      		<form:option value="${tipoCargo}">${tipoCargo}</form:option>
				      	</c:forEach>
				      </form:select>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Rua</label>
				    <div class="col-sm-3">
				      <form:input type="text" required = "rua" class="form-control" placeholder="Rua" path="endereco.nomeRua" id="rua" pattern="[A-Za-z�-� ']+$" value="${funcionario.endereco.nomeRua}"/>
				    </div>
				    <label class="col-sm-1 control-label">Numero</label>
				    <div class="col-sm-2">
				      <form:input type="text" required="numero" maxlength="5" class="form-control" placeholder="Numero" path="endereco.numero" id="numero" pattern="[a-zA-Z 0-9]+$" value="${funcionario.endereco.numero}"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">CEP</label>
				    <div class="col-sm-6">
				      <form:input type="text" maxlength="8" minlength="8" required = "cep" class="form-control" placeholder="CEP" path="endereco.cep" id="cep" pattern="[0-9]+$" value="${funcionario.endereco.cep}"/>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Bairro</label>
				    <div class="col-sm-6">
				      <form:input type="text" required = "bairro" class="form-control" placeholder="Bairro" path="endereco.bairro" id="bairro" pattern="[A-Za-z�-� ']+$" value="${funcionario.endereco.bairro}"/>
				  	</div>
				  </div>

				  <div class="form-group">
				    <label class="col-sm-3 control-label">Estado</label>
				    <div class="col-sm-6">
					  <form:select class="form-control" path="endereco.cidade.estado.nomeEstado">
					  <c:forEach items="${listaEstados}" var="estado">
				      	<form:option value="${estado.nomeEstado}">${estado.nomeEstado}</form:option>
				      </c:forEach>
				      </form:select>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Cidade</label>
				    <div class="col-sm-6">
				      <form:input type="text" required = "cidade" class="form-control" placeholder="Cidade" path="endereco.cidade.cidade" id="cidade" pattern="[A-Za-z�-� ']+$" value="${funcionario.endereco.cidade.cidade}"/>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">DDD</label>
				    <div class="col-sm-1">
				      <form:input type="text" maxlength="3" minlength="2" class="form-control" placeholder="DDD" path="telefones[0].ddd" id="ddd" pattern="[0-9]+$" value="${funcionario.telefones[0].ddd}"/>
				    </div>
				    <label class="col-sm-1 control-label">Telefone</label>
				    <div class="col-sm-3">
				      <form:input type="text" class="form-control" required = "numeroTelefone" placeholder="Telefone" path="telefones[0].numeroTelefone" id="telefone" maxlength="9" minlength="8" pattern="[0-9]+$" data-mask="00000000" data-mask-selectonfocus="true" value="${funcionario.telefones[0].numeroTelefone}"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">E-mail</label>
				    <div class="col-sm-6">
				      <form:input type="text" required = "email" class="form-control" placeholder="E-mail" path="login.email" id="email" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="${funcionario.login.email}"/>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Login</label>
				    <div class="col-sm-6">
				      <form:input type="text" class="form-control" placeholder="Login" path="login.login" value="${funcionario.login.login}"/>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Senha</label>
				    <div class="col-sm-6">
				      <form:password class="form-control" placeholder="Senha" path="login.senha" value="${funcionario.login.senha}"/>
				  	</div>
				  </div>
				  <center>
					  <input type="submit" class="btn btn-primary" value="Alterar" name="alterar"/> 
					  <input type="submit" class="btn btn-danger" value="Excluir" name="excluir"/>
				  </center>
            </form:form>
        </div>
        
</tags:pageTemplate>