<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form"  prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<tags:pageTemplate titulo="Cadastro de Funcion�rio">
       	<script src=<c:url value="/resources/js/util.js"/>></script>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cadastro de Funcion�rio</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <form:form class="form-horizontal" method="post" commandName="funcionario" action="${s:mvcUrl('FC#cadastrar').build()}">
            	  <div class="form-group">
				    <label class="col-sm-3 control-label">Nome</label>
				    <div class="col-sm-6">
			    		  <form:input type="text" id="nome" name="nome" required= "Nome" class="form-control" path="nome" placeholder="Nome" pattern="[A-Za-z�-� ']+$" />
				    </div>
				  </div>
			      <div class="form-group">
				    <label class="col-sm-3 control-label">Sobrenome</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="sobrenomeF" name="sobrenomePJ" required = "Sobrenome" class="form-control" placeholder="Sobrenome"  path="sobrenome" pattern="[A-Za-z�-� ']+$"/>
				    </div>
				  </div>	  
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Data de nascimento</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="dataNasc" name="dataNasc" required = "dataNascimento" class="form-control" placeholder="Exemplo: DD/MM/AAAA" path="dataNascimento" pattern="[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" min="1930-01-01" max="2014-02-18" onkeypress="mascaraData()" maxlength="10"/>
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">RG</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="rg" name= "rg" maxlength="10" required = "rg"  class="form-control" placeholder="RG" path="rg" pattern="[a-zA-Z0-9]+" />
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">CPF</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="cpf" name="cpf" maxlength="11" minlength="11" required = "cpf"  class="form-control" placeholder="CPF" path="cpf" pattern="[0-9]+$" />
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Registro OAB</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="oab" name="oab" maxlength="8" required = "oab"  class="form-control" placeholder="Registro OAB" path="numeroOAB" pattern="[0-9]+$" />
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Cargo</label>
				    <div class="col-sm-6">
				      <form:select id="formCargo" name="formCargo" class="form-control" path="tipoFuncionario">
				      	<c:forEach items="${cargos}" var="cargo">
				      		<form:option value="${cargo}">${cargo}</form:option>
				      	</c:forEach>
				      </form:select>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Rua</label>
				    <div class="col-sm-3">
				      <form:input type="text" required = "rua" id="rua" name="rua" class="form-control" placeholder="Rua" path="endereco.nomeRua" pattern="[A-Za-z�-� ']+$"/>
				    </div>
				    <label class="col-sm-1 control-label">Numero</label>
				    <div class="col-sm-2">
				      <form:input type="text" maxlength="5" required = "numero" id="numero" name="numero" class="form-control" placeholder="Numero" path="endereco.numero" pattern="[a-zA-Z 0-9]+$"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">CEP</label>
				    <div class="col-sm-6">
				      <form:input type="text" maxlength="8" minlength="8" required = "cep"  id="cep" name="cep" class="form-control" placeholder="CEP" path="endereco.cep" pattern="[0-9]+$"/>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Bairro</label>
				    <div class="col-sm-6">
				      <form:input type="text" required = "bairro"  class="form-control" placeholder="Bairro" id="bairro" name = "bairro" path="endereco.bairro" pattern="[A-Za-z�-� ']+$" />
				  	</div>
				  </div>

				  <div class="form-group">
				    <label class="col-sm-3 control-label">Estado</label>
				    <div class="col-sm-6">
					  <form:select id="formEstado" name="formEstado" class="form-control" path="endereco.cidade.estado.nomeEstado">
					  <c:forEach items="${listaEstados}" var="estado">
				      	<form:option value="${estado.nomeEstado}">${estado.nomeEstado}</form:option>
				      </c:forEach>
				      </form:select>
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Cidade</label>
				    <div class="col-sm-6">
				      <form:input type="text" id="cidade" name="cidade" required = "cidade"  class="form-control" placeholder="Cidade" path="endereco.cidade.cidade" pattern="[A-Za-z�-� ']+$" />
				  	</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">DDD</label>
				    <div class="col-sm-1">
				      <form:input type="text" id="ddd" name="ddd" required="ddd" maxlength="3" minlength="2" class="form-control" placeholder="DDD" path="telefones[0].ddd" pattern="[0-9]+$"/>
				    </div>
				    <label class="col-sm-1 control-label">Telefone</label>
				    <div class="col-sm-3">
				      <form:input type="text" id="telefone" name="telefone" required = "numeroTelefone" maxlength="9" minlength="8" pattern="[0-9]+$" data-mask="00000000" data-mask-selectonfocus="true" class="form-control" placeholder="Telefone" path="telefones[0].numeroTelefone"/>
				    </div>
				  </div>	
				  <div class="form-group">
				    <label class="col-sm-3 control-label">E-mail</label>
				    <div class="col-sm-6">
				      <form:input type="email" id="email" name="email" required = "email"  class="form-control" placeholder="Email" path="login.email" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"/>
				  	</div>
				  </div>
				  <!--  
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Login</label>
				    <div class="col-sm-6">
				      <form:input type="text" class="form-control" placeholder="Login" path="login.login"/>
				  	</div>
				  </div>
				  -->
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Senha</label>
				    <div class="col-sm-6">
				      <form:password id="senha" name="senha" class="form-control" required = "senha"  placeholder="Senha" path="login.senha"/>
				  	</div>
				  </div>
				  <center>
					  <input type="submit" id="btnCadastrar" name="btnCadastrar" class="btn btn-primary" value="Cadastrar"> 
					  <button type="reset" class="btn btn-danger">Limpar</button>
				  </center>
            </form:form>
        </div>
</tags:pageTemplate>