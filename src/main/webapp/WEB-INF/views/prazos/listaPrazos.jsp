<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<tags:pageTemplate titulo="Lista de Prazos Processuais C�veis">
	<div id="page-wrapper">
    	<div class="row">
        	<div class="col-lg-12">
            	<h1 class="page-header">Lista de Prazos Processuais C�veis</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="table-responsive">
 		<table class="table table-bordered table-striped">
    		<tr>
    			<th>Par�grafo</th>
    			<th>Prazo</th>
    			<th>Descri��o</th>
    			<th>Tipo</th>
    		</tr>
    		<c:forEach items="${listaPrazos}" var="prazos">
	    		<tr>

		    			<td>${prazos.paragrafo}</td>
		    			<td>${prazos.prazo}</td>
		    			<td>${prazos.descricao}</td>
		    			<td>${prazos.tipoProcesso}</td>
	
	    		</tr>
			</c:forEach>
  		</table>
	</div>
</tags:pageTemplate>