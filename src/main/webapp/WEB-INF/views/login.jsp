<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>AUJURI - LOGIN</title>
	<link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/bootstrap.min.css"/>>
	<link rel="stylesheet" type="text/css" href=<c:url value="/resources/css/bootstrap-theme.min.css"/>>
    <style type="text/css">
        body{
            padding: 60px 0px;
        }
    </style>
</head>
<body>
    <div class="container">
        <center><h1>Login AUJURI</h1></center>
        <form:form servletRelativeAction="/login" method="post">
            <div class="form-group">
                <label>E-mail</label>
                <input type="text" name="username" class="form-control" />
            </div>
            <div class="form-group">
                <label>Senha</label>
                <input type="password" name="password" class="form-control" />
            </div>
            <button type="submit" class="btn btn-primary">Logar</button>
        </form:form>
    </div>
</body>
</html>